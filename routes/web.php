<?php

//Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('logout');

Auth::routes();

Route::get('/', 'Front\DashboardController@index')->name('front.index');
Route::get('/boardGames', 'Front\BoardGamesController@index')->name('front.board_games.index');
Route::get('/shoppingLists', 'Front\ShoppingListsController@index')->name('front.shopping_lists.index');


Route::prefix('admin')->group(function()
{
    Route::get('login', 'Auth\AdminLoginController@loginForm')->name('admin.login');
    Route::post('login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

    Route::get('/', 'Admin\DashboardController@index')->name('admin.dashboard');

    Route::resource('account','Admin\AccountController', AdminRouteHelper::RouteResource('account'));

    Route::post('urls/filters', 'Admin\UrlsController@filters')->name('admin.urls.filters.submit');
    Route::post('urls/ajaxCheckUrl', 'Admin\UrlsController@ajaxCheckUrl')->name('admin.urls.ajaxCheckUrl');
    Route::post('urls/ajaxGenerateUrl', 'Admin\UrlsController@ajaxGenerateUrl')->name('admin.urls.ajaxGenerateUrl');
    Route::post('urls/ajaxGetMenu', 'Admin\UrlsController@ajaxGetMenu')->name('admin.urls.ajaxGetMenu');
    Route::resource('urls','Admin\UrlsController', AdminRouteHelper::RouteResource('urls'));

    Route::resource('pages','Admin\PagesController', AdminRouteHelper::RouteResource('pages'));

    Route::post('menus/ajaxGetMenus', 'Admin\MenusController@ajaxGetMenus')->name('admin.menus.ajaxGetMenus');
    Route::resource('menus','Admin\MenusController', AdminRouteHelper::RouteResource('menus'));
    Route::resource('modules','Admin\ModulesController', AdminRouteHelper::RouteResource('modules'));
    Route::resource('admins','Admin\UsersAdminsController', AdminRouteHelper::RouteResource('admins'));


    // ----

    Route::resource('notes','Admin\NotesController', AdminRouteHelper::RouteResource('notes'));
    Route::resource('notesGroups','Admin\NotesGroupsController', AdminRouteHelper::RouteResource('notes_groups'));
    Route::post('shoppingLists/filters', 'Admin\ShoppingListsController@filters')->name('admin.shopping_lists.filters.submit');
    Route::post('shoppingLists/search', 'Admin\ShoppingListsController@search')->name('admin.shopping_lists.search.submit');
    Route::post('shoppingLists/ajaxQuantityPriority', 'Admin\ShoppingListsController@ajaxQuantityPriority')->name('admin.shopping_lists.ajax_quantity_priority.submit');
    Route::get('shoppingLists/upDown/{action}/{id}', 'Admin\ShoppingListsController@upDown')->name('admin.shopping_lists.up_down');
    Route::resource('shoppingLists','Admin\ShoppingListsController', AdminRouteHelper::RouteResource('shopping_lists'));
    Route::resource('shoppingListsShops','Admin\ShoppingListsShopsController', AdminRouteHelper::RouteResource('shopping_lists_shops'));
    Route::resource('boardGames','Admin\BoardGamesController', AdminRouteHelper::RouteResource('board_games'));
    Route::post('boardGames/filters', 'Admin\BoardGamesController@filters')->name('admin.board_games.filters.submit');
    Route::resource('boardGamesPlays','Admin\BoardGamesPlaysController', AdminRouteHelper::RouteResource('board_games_plays'));
    Route::post('boardGamesPlays/filters', 'Admin\BoardGamesPlaysController@filters')->name('admin.board_games_plays.filters.submit');
    Route::get('boardGamesPlays/filtersGame/{id}', 'Admin\BoardGamesPlaysController@filtersGame')->name('admin.board_games_plays.filters.game');
    Route::resource('boardGamesPlayers','Admin\BoardGamesPlayersController', AdminRouteHelper::RouteResource('board_games_players'));
    Route::resource('boardGamesStats','Admin\BoardGamesStatsController', AdminRouteHelper::RouteResource('board_games_stats'));
    Route::post('boardGamesStats/filters', 'Admin\BoardGamesStatsController@filters')->name('admin.board_games_stats.filters.submit');
    Route::resource('payments','Admin\PaymentsController', AdminRouteHelper::RouteResource('payments'));
});
