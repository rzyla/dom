<?php

use Illuminate\Database\Seeder;

class FirstAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_admins')->insert([
            'name' => 'Radosław Żyła',
            'email' => 'radek@radek.pl',
            'acl' => 9,
            'password' => Hash::make('radek123'),
        ]);
    }
}
