<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingListsShopsListsTable extends Migration
{
    public function up()
    {
        Schema::create('shopping_lists_shops_lists', function (Blueprint $table) {
            $table->integer('shopping_lists_shops_id');
            $table->integer('shopping_lists_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('shopping_lists_shops_lists');
    }
}
