<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board_games', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('board_game_id')->default(0)->nullable($value = 0);
            $table->unsignedInteger('type')->default(0)->nullable($value = 0);
            $table->string('name', 150)->nullable();
            $table->unsignedInteger('extension')->default(0)->nullable($value = 0);
            $table->unsignedInteger('wishlist')->default(0)->nullable($value = 0);
            $table->text('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('board_games');
    }
}
