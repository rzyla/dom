<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('type')->default(0)->nullable($value = 0);
            $table->string('name', 100);
            $table->unsignedInteger('month_01')->default(0)->nullable($value = 0);
            $table->unsignedInteger('month_02')->default(0)->nullable($value = 0);
            $table->unsignedInteger('month_03')->default(0)->nullable($value = 0);
            $table->unsignedInteger('month_04')->default(0)->nullable($value = 0);
            $table->unsignedInteger('month_05')->default(0)->nullable($value = 0);
            $table->unsignedInteger('month_06')->default(0)->nullable($value = 0);
            $table->unsignedInteger('month_07')->default(0)->nullable($value = 0);
            $table->unsignedInteger('month_08')->default(0)->nullable($value = 0);
            $table->unsignedInteger('month_09')->default(0)->nullable($value = 0);
            $table->unsignedInteger('month_10')->default(0)->nullable($value = 0);
            $table->unsignedInteger('month_11')->default(0)->nullable($value = 0);
            $table->unsignedInteger('month_12')->default(0)->nullable($value = 0);
            $table->timestamps();
        });

        Schema::create('payments_paids', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('payments_id');
            $table->unsignedInteger('month');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('payments');
        Schema::dropIfExists('payments_paids');
    }
}
