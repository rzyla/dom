<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration
{
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('notes_groups_id')->default(0);
            $table->string('title', 150)->nullable();
            $table->text('description')->nullable();
            $table->integer('dashboard')->default(0)->nullable($value = 0);
            $table->timestamps();
        });

        //Schema::create('notes', function (Blueprint $table) {
        //    $table->foreign('notes_groups_id')->references('id')->on('notes_groups')
        //    ->onUpdate('cascade')->onDelete('cascade');
        //});
    }

    public function down()
    {
        Schema::dropIfExists('notes');
    }
}
