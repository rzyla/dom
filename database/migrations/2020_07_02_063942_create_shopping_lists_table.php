<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingListsTable extends Migration
{
    public function up()
    {
        Schema::create('shopping_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150)->nullable();
            $table->string('lead', 250)->nullable();
            $table->text('description')->nullable();
            $table->text('image')->nullable();
            $table->integer('quantity')->default(0)->nullable($value = 0);
            $table->integer('prio')->default(0)->nullable($value = 0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('shopping_lists');
    }
}
