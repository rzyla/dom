<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUrsSeoTable extends Migration
{
    public function up()
    {
        Schema::create('urls_seo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('languages_id');
            $table->integer('modules_id');
            $table->integer('modules_items_id');
            $table->string('url', 250)->default(null)->nullable();
            $table->string('meta_title', 150)->default(null)->nullable();
            $table->string('meta_description', 250)->default(null)->nullable();
            $table->string('meta_key_words', 150)->default(null)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('urls_seo');
    }
}
