<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardGamesPlaysScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board_games_plays_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('board_games_plays_id')->default(0)->nullable($value = 0);
            $table->unsignedInteger('board_games_players_id')->default(0)->nullable($value = 0);
            $table->unsignedInteger('score')->default(0)->nullable($value = 0);
            $table->unsignedInteger('win')->default(0)->nullable($value = 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('board_games_plays_scores');
    }
}
