let mix = require('laravel-mix');
const del = require('del');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
 
mix.sass('resources/assets/sass/admin/admin.scss', 'public/assets/admin/css/temp.css');

mix.styles([
   'vendor/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css', 
   'vendor/almasaeed2010/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css', 
   'vendor/almasaeed2010/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css', 
   'vendor/almasaeed2010/adminlte/plugins/jqvmap/jqvmap.min.css', 
   'vendor/almasaeed2010/adminlte/dist/css/adminlte.min.css', 
   'vendor/almasaeed2010/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css', 
   'vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css', 
   'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', 
   //'vendor/almasaeed2010/adminlte/plugins/summernote/summernote-bs4.css', 
   'public/assets/admin/css/temp.css', 
], 'public/assets/admin/css/admin.css').then(() => {
    del('public/assets/admin/css/temp.css'); 
});

mix.scripts([
   'vendor/almasaeed2010/adminlte/plugins/jquery/jquery.min.js', 
   'vendor/almasaeed2010/adminlte/plugins/jquery-ui/jquery-ui.min.js', 
   'vendor/almasaeed2010/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js', 
   'vendor/almasaeed2010/adminlte/plugins/chart.js/Chart.min.js', 
   'vendor/almasaeed2010/adminlte/plugins/sparklines/sparkline.js', 
   'vendor/almasaeed2010/adminlte/plugins/jqvmap/jquery.vmap.min.js', 
   'vendor/almasaeed2010/adminlte/plugins/jqvmap/maps/jquery.vmap.usa.js', 
   'vendor/almasaeed2010/adminlte/plugins/jquery-knob/jquery.knob.min.js', 
   'vendor/almasaeed2010/adminlte/plugins/moment/moment.min.js', 
   'vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js', 
   'vendor/almasaeed2010/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js', 
   //'vendor/almasaeed2010/adminlte/plugins/summernote/summernote-bs4.min.js', 
   'vendor/almasaeed2010/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js', 
   'resources/assets/js/vendor/jquery.ba-throttle-debounce.min.js',  
   'vendor/almasaeed2010/adminlte/dist/js/adminlte.min.js',  
   'resources/assets/js/admin.js',  
], 'public/assets/admin/js/admin.js')

mix.copyDirectory('vendor/almasaeed2010/adminlte/plugins/summernote', 'public/assets/admin/plugins/summernote')
mix.copyDirectory('vendor/almasaeed2010/adminlte/plugins/fontawesome-free/webfonts', 'public/assets/admin/webfonts')
