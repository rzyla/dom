<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class BoardGamesPlayers extends Model
{
    use Notifiable;

    protected $guard = 'admin';
    protected $table = 'board_games_players';

    protected $fillable =
        [
            'name'
        ];
}
