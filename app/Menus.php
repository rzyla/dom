<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Menus extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';
    protected $table = 'menus';

    protected $fillable = 
    [
        'name', 'symbol', 'languages_id'
    ];
    
    public static function getMenus($languages_id = null, $withLangLabel = false, $ajax = false)
    {
        $db = Menus::select('menus.name', 'menus.id', 'languages.id as languages_id', 'languages.name as languages_name')
                    ->join('languages', 'menus.languages_id', '=', 'languages.id')
                    ->get();

        $menus = [];

        foreach($db as $menu)
        {
            if($menu['languages_id'] == $languages_id || $languages_id == null)
            {
                $menus[$menu->id] = empty($withLangLabel) ? $menu->name : $menu->name.' ('.$menu->languages_name.')';
            }
        }

        return $menus;
    }
}
