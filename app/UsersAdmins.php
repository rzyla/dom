<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UsersAdmins extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';
    
    protected $fillable = 
    [
        'name', 'email', 'password', 'avatar', 'acl'
    ];

    protected $hidden = 
    [
        'password', 'remember_token',
    ];
}
