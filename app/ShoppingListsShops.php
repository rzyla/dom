<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class ShoppingListsShops extends Model
{
    use Notifiable;

    protected $guard = 'admin';
    protected $table = 'shopping_lists_shops';

    protected $fillable = 
    [
        'name'
    ];
}
