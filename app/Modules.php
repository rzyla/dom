<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Modules extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';

    protected $fillable = 
    [
        'name', 'class', 'symbol', 'singleton'
    ];

    public static function getModules($array = false, $ajax = false)
    {
        $db = Modules::all();
        $modules = [];

        foreach($db as $module)
        {
            if(empty($array))
            {
                $modules[$module->id] = $module->name;
            }
            else
            {
                $modules[$module->id] = 
                [
                    'name' => $module->name,
                    'available' => true // jesli singleton to trzeba kontrolowac czy moze byc dołaczony
                ];
            }
        }

        return $modules;
    }

    public static function getModuleIdBySymbol($symblol)
    {
        $module = Modules::where('symbol', '=', $symblol)->first();
        
        return !empty($module) ? $module->id : null;
    }
}
