<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class BoardGamesPlays extends Model
{
    use Notifiable;

    protected $guard = 'admin';
    protected $table = 'board_games_plays';

    protected $fillable =
        [
            'board_game_id',
            'datetime',
            'type',
            'win',
            'no_score'
        ];
}
