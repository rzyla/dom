<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Urls extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';

    protected $fillable = 
    [
        'name', 'languages_id', 'menus_id', 'parent_id', 'modules_id', 'modules_items_id', 'url', 'meta_title', 'meta_description', 'meta_key_words'
    ];

    public static function getUrlsAdminGrid($prefix)
    {
        $filters = session('filters');
        $filter_languages = isset($filters[$prefix.'_languages']) ? $filters[$prefix.'_languages'] : null;
        $filter_menus = isset($filters[$prefix.'_menus']) ? $filters[$prefix.'_menus'] : null;
        $filter_modules = isset($filters[$prefix.'_modules']) ? $filters[$prefix.'_modules'] : null;
        $filter_pagination = isset($filters[$prefix.'_pagination']) ? $filters[$prefix.'_pagination'] : null;

        $urls = Urls::select('*');

        if(!empty($filter_languages))
        {
            $urls->where('languages_id', '=', $filter_languages);
        }

        if(!empty($filter_menus))
        {
            $urls->where('menus_id', '=', $filter_menus);
        }

        if(!empty($filter_modules))
        {
            $urls->where('modules_id', '=', $filter_modules);
        }

        $return = empty($filter_pagination) ? $urls->get() : $urls->paginate($filter_pagination);

        return $return;
    }

    public static function getAdminGrid($languages_id, $menu_id, $parent_id, $level = 0)
    {
        $return = Urls::where('languages_id', '=', $languages_id)->where('menus_id', '=', $menu_id)->where('parent_id', '=', $parent_id)->get();

        if(!empty($return))
        {
            $level++;
            $return = $return->toArray();

            foreach($return as $key => $row)
            {
                $route = '';

                if(!empty($row['modules_id']) && !empty($row['modules_items_id']))
                {
                    $module = Modules::find($row['modules_id']);
                    $route = route('admin.'.$module->symbol.'.edit', $row['modules_items_id']);
                }

                $return[$key]['route']  = $route;
                $return[$key]['level']  = $level;
                $return[$key]['items'] = Urls::getAdminGrid($languages_id, $menu_id, $row['id'], $level);
            }
        }

        return $return;
    }

    public static function getRoots($menu_id = '', $without = '')
    {
        $array = [];
        
        Urls::getRootsRecursively($array, 0, $menu_id, $without);

        return $array;
    }

    public static function getRootsRecursively(&$array, $parent_id, $menu_id = '', $without = '', $string = '')
    {
        $db = Urls::where('parent_id', '=', $parent_id);

        if(!empty($menu_id))
        {
            $db->where('menus_id', '=', $menu_id);
        }

        if(!empty($without))
        {
            $db->where('id', '!=', $without);
        }

        $urls = $db->get();

        foreach($urls as $url)
        {
            $array[$url->id] = !empty($parent_id) ? $string . ' > '. $url->name : $url->name ;

            Urls::getRootsRecursively($array, $url->id, $menu_id, $without, $array[$url->id]);
        }
    }

    public static function getRowByModuleItemId($modules_id = '', $modules_items_id = '')
    {
        $return = [];

        if(!empty($modules_id) && !empty($modules_items_id))
        {
            $module = Urls::where('modules_id', '=', $modules_id)->where('modules_items_id', '=', $modules_items_id)->first();

            if(!empty($module))
            {
                $return = $module->toArray();
            }
        }

        return $return;
    }

    public static function getIdByModuleItemId($modules_id = '', $modules_items_id = '')
    {
        $module = Urls::getRowByModuleItemId($modules_id, $modules_items_id);

        return !empty($module['id']) ? $module['id'] : '';
    }

    public static function updateUrlMetaByModuleItemId($input, $modules_id = '', $modules_items_id = '')
    {
        $urls =  Urls::where('modules_id', '=', $modules_id)->where('modules_items_id', '=', $modules_items_id)->first();

        $array = 
        [
            'url' => $input['url'],
            'meta_title' => $input['meta_title'],
            'meta_description' => $input['meta_description'],
            'meta_key_words' => $input['meta_key_words'],
        ];

        $urls->update($array);
    }
}
