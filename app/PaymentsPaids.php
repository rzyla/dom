<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\NotesGroups;

class PaymentsPaids extends Model
{
    use Notifiable;

    protected $guard = 'admin';
    protected $table = 'payments_paids';

    protected $fillable =
    [
        'payments_id', 'month'
    ];
}
