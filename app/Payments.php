<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\NotesGroups;

class Payments extends Model
{
    use Notifiable;

    protected $guard = 'admin';
    protected $table = 'payments';

    protected $fillable =
    [
        'type', 'name', 'month_01', 'month_02', 'month_03', 'month_04', 'month_05', 'month_06', 'month_07', 'month_08', 'month_09', 'month_10', 'month_11', 'month_12'
    ];
}
