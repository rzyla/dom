<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class ShoppingLists extends Model
{
    use Notifiable;

    protected $guard = 'admin';
    protected $table = 'shopping_lists';

    protected $fillable =
    [
        'name',
        'lead',
        'description',
        'image',
        'quantity',
        'prio'
    ];
}
