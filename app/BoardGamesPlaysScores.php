<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class BoardGamesPlaysScores extends Model
{
    use Notifiable;

    protected $guard = 'admin';
    protected $table = 'board_games_plays_scores';

    protected $fillable =
        [
            'board_games_plays_id',
            'board_games_players_id',
            'score',
            'win'
        ];
}
