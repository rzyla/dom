<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class BoardGames extends Model
{
    use Notifiable;

    protected $guard = 'admin';
    protected $table = 'board_games';

    protected $fillable =
        [
            'board_game_id',
            'type',
            'name',
            'extension',
            'wishlist',
            'image'
        ];
}
