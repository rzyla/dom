<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ShopingListsPositions extends Model
{
    use Notifiable;

    protected $guard = 'admin';
    protected $table = 'shopping_lists_positions';

    protected $fillable =
        [
            'shopping_lists_id',
            'shopping_lists_shops_id',
            'position'
        ];
}
