<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class NotesGroups extends Model
{
    use Notifiable;

    protected $guard = 'admin';
    protected $table = 'notes_groups';

    protected $fillable = 
    [
        'name'
    ];
}
