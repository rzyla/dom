<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class ShoppingListsShopsShoppingLists extends Model
{
    use Notifiable;

    protected $guard = 'admin';
    protected $table = 'shopping_lists_shops_lists';

    protected $fillable = 
    [
        'shopping_lists_shops_id',
        'shopping_lists_id'
    ];
}