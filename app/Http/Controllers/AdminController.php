<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use App\Languages;
use App\Modules;
use App\Urls;

class AdminController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $app = [];
    protected $acl = [];
    protected $filters = [];
    protected $search = false;
    protected $prefix;
    protected $uploadSourceDir;
    protected $uploadThumbsDir;

    protected $paginationSizes =
    [
        5 => '5 wyników',
        10 => '10 wyników',
        20 => '20 wyników',
        50 => '50 wyników',
        100 => '100 wyników'
    ];

    const aclGuest = 0;
    const aclUser = 5;
    const aclAdmin = 8;
    const aclSuperAdmin = 9;

    function __construct($prefix = null, $buttons = ['create'])
    {
        if(!empty($prefix))
        {
            $this->prefix = $prefix;

            $this->app['buttons']['index'] = route('admin.'.$prefix.'.index');

            if(in_array('create', $buttons))
            {
                $this->app['buttons']['create'] = route('admin.'.$prefix.'.create');
            }

            if(in_array('search', $buttons))
            {
                $this->app['buttons']['search'] = route('admin.'.$prefix.'.search.submit');
            }

            if(in_array('filters', $buttons))
            {
                $this->app['buttons']['filters'] = route('admin.'.$prefix.'.filters.submit');
            }

            $this->uploadSourceDir = public_path('uploads/') . $prefix;
            $this->uploadThumbsDir = public_path('thumbs/uploads/') . $prefix;

            $this->app['current']['uploadSourceDir'] = 'uploads/' . $prefix . '/';
        }
    }

    protected function init()
    {
        $this->initAclData();
        $this->initUserData();
        $this->initMenuData();
        $this->initMetaData();
        $this->initBreadcrumbData();
        $this->initSearch();
    }

    protected function initAclData()
    {
        $this->acl =
        [
            self::aclGuest => "Użytkownik",
            self::aclUser => "Super użytkownik",
            self::aclAdmin => "Administrator",
            self::aclSuperAdmin => "Super administrator",
        ];
    }

    private function initUserData()
    {
        $this->app['user'] = Auth::user();

        if(empty($this->app['user']['avatar']))
        {
            $this->app['user']['avatar'] = 'assets/admin/images/avatar.png';
        }
        else
        {
            $this->app['user']['avatar'] = 'uploads/account/'.$this->app['user']['avatar'];
        }
    }

    private function checkAcl($acl)
    {
        if(!in_array($this->app['user']['acl'], $acl))
        {
            $this->redirectDashboard();
        }
    }

    protected function redirectDashboard()
    {
        header('Location:'. route('admin.dashboard'));
        exit;
    }

    private function initMenuData()
    {
        if(!empty(request()->segment(3)))
        {
            $current_url = route('admin.dashboard') . '/' .  request()->segment(2) . '/' . request()->segment(3);
        }
        elseif(!empty(request()->segment(2)))
        {
            $current_url = route('admin.dashboard') . '/' .  request()->segment(2);
        }
        else
        {
            $current_url = route('admin.dashboard');
        }

        if(!empty(request()->segment(2)))
        {
            $second_current_url = route('admin.dashboard') . '/' .  request()->segment(2);
        }
        else
        {
            $second_current_url = route('admin.dashboard');
        }

        $this->app['menu'][] =
        [
            'name' => 'Strona główna',
            'route' => route('admin.dashboard'),
            'icon' => 'fa-tachometer-alt',
            'visible' => true,
            'dashboard' => true,
            'acl' => [self::aclGuest, self::aclUser, self::aclAdmin, self::aclSuperAdmin],
            'children' => [],
        ];

        $this->app['menu'][] =
        [
            'name' => 'Moje konto',
            'route' => route('admin.account.index'),
            'icon' => 'fa-user',
            'visible' => false,
            'dashboard' => false,
            'acl' =>  [self::aclGuest, self::aclUser, self::aclAdmin, self::aclSuperAdmin],
            'children' => [],
        ];

        $this->app['menu'][] =
        [
            'name' => 'Mapa strony',
            'route' => route('admin.urls.index'),
            'icon' => 'fa-sitemap',
            'visible' => true,
            'dashboard' => true,
            'acl' =>  [self::aclGuest, self::aclUser, self::aclAdmin, self::aclSuperAdmin],
            'children' => []
        ];

        $children = [];
        $children[] =
        [
            'name' => 'Strony tekstowe',
            'route' => route('admin.pages.index'),
            'icon' => 'fa-edit',
            'visible' => true,
            'dashboard' => true,
            'acl' =>  [self::aclGuest, self::aclUser, self::aclAdmin, self::aclSuperAdmin],
            'children' => []
        ];

        $this->app['menu'][] =
        [
            'name' => 'Moduły',
            'route' => '#',
            'icon' => 'fa-layer-group',
            'visible' => true,
            'dashboard' => true,
            'acl' => [self::aclGuest, self::aclUser, self::aclAdmin, self::aclSuperAdmin],
            'children' => $children
        ];

        $this->app['menu'][] =
        [
            'name' => 'Płatności',
            'route' => route('admin.payments.index'),
            'icon' => 'fa-credit-card',
            'visible' => true,
            'dashboard' => true,
            'acl' =>  [self::aclGuest, self::aclUser, self::aclAdmin, self::aclSuperAdmin],
            'children' => []
        ];

        // ShoppingLists
        $children = [];
        $children[] =
        [
            'name' => 'Lista produktów',
            'route' => route('admin.shopping_lists.index'),
            'icon' => 'fa-bars',
            'visible' => true,
            'dashboard' => false,
            'acl' => [self::aclAdmin, self::aclSuperAdmin],
            'children' => [],
        ];
        $children[] =
        [
            'name' => 'Lista sklepów',
            'route' => route('admin.shopping_lists_shops.index'),
            'icon' => 'fa-layer-group',
            'visible' => true,
            'dashboard' => false,
            'acl' => [self::aclAdmin, self::aclSuperAdmin],
            'children' => [],
        ];

        $this->app['menu'][] =
        [
            'name' => 'Listy zakupowe',
            'route' => '#',
            'icon' => 'fa-shopping-basket',
            'visible' => true,
            'dashboard' => true,
            'acl' => [self::aclAdmin, self::aclSuperAdmin],
            'children' => $children
        ];

        // Notes
        $children = [];
        $children[] =
        [
            'name' => 'Dodaj notatkę',
            'route' => route('admin.notes.create'),
            'icon' => 'fa-plus',
            'visible' => true,
            'dashboard' => false,
            'acl' => [self::aclAdmin, self::aclSuperAdmin],
            'children' => [],
        ];
        $children[] =
        [
            'name' => 'Lista notatek',
            'route' => route('admin.notes.index'),
            'icon' => 'fa-bars',
            'visible' => true,
            'dashboard' => false,
            'acl' => [self::aclAdmin, self::aclSuperAdmin],
            'children' => [],
        ];
        $children[] =
        [
            'name' => 'Lista grup notatek',
            'route' => route('admin.notes_groups.index'),
            'icon' => 'fa-layer-group',
            'visible' => true,
            'dashboard' => false,
            'acl' => [self::aclAdmin, self::aclSuperAdmin],
            'children' => [],
        ];

        $this->app['menu'][] =
        [
            'name' => 'Notatki',
            'route' => '#',
            'icon' => 'fa-edit',
            'visible' => true,
            'dashboard' => true,
            'acl' => [self::aclAdmin, self::aclSuperAdmin],
            'children' => $children
        ];

        // Games
        $children = [];
        $children[] =
            [
                'name' => 'Lista gier',
                'route' => route('admin.board_games.index'),
                'icon' => 'fa-bars',
                'visible' => true,
                'dashboard' => false,
                'acl' => [self::aclAdmin, self::aclSuperAdmin],
                'children' => [],
            ];
        $children[] =
            [
                'name' => 'Lista wyników',
                'route' => route('admin.board_games_plays.index'),
                'icon' => 'fa-trophy',
                'visible' => true,
                'dashboard' => false,
                'acl' => [self::aclAdmin, self::aclSuperAdmin],
                'children' => [],
            ];
        $children[] =
            [
                'name' => 'Statystyki',
                'route' => route('admin.board_games_stats.index'),
                'icon' => 'fa-calendar-alt',
                'visible' => true,
                'dashboard' => false,
                'acl' => [self::aclAdmin, self::aclSuperAdmin],
                'children' => [],
            ];
        $children[] =
            [
                'name' => 'Gracze',
                'route' => route('admin.board_games_players.index'),
                'icon' => 'fa-user-friends',
                'visible' => true,
                'dashboard' => false,
                'acl' => [self::aclAdmin, self::aclSuperAdmin],
                'children' => [],
            ];

        $this->app['menu'][] =
            [
                'name' => 'Gry planszowe',
                'route' => '#',
                'icon' => 'fa-dice-d20',
                'visible' => true,
                'dashboard' => true,
                'acl' => [self::aclAdmin, self::aclSuperAdmin],
                'children' => $children
            ];

        $children = [];

        if($this->app['user']['acl'] == self::aclSuperAdmin)
        {
            $children[] =
            [
                'name' => 'Menu',
                'route' => route('admin.menus.index'),
                'icon' => 'fa-bars',
                'visible' => true,
                'dashboard' => false,
                'acl' => [self::aclSuperAdmin],
                'children' => [],
            ];

            $children[] =
            [
                'name' => 'Moduły',
                'route' => route('admin.modules.index'),
                'icon' => 'fa-cube',
                'visible' => true,
                'dashboard' => false,
                'acl' => [self::aclSuperAdmin],
                'children' => [],
            ];
        }

        if($this->app['user']['acl'] >= self::aclAdmin)
        {
            $children[] =
            [
                'name' => 'Administratorzy',
                'route' => route('admin.admins.index'),
                'icon' => 'fa-users',
                'visible' => true,
                'dashboard' => false,
                'acl' => [self::aclAdmin, self::aclSuperAdmin],
                'children' => [],
            ];
        }

        $this->app['menu'][] =
        [
            'name' => 'Administracja',
            'route' => '#',
            'icon' => 'fa-users-cog',
            'visible' => true,
            'dashboard' => true,
            'acl' => [self::aclUser, self::aclAdmin, self::aclSuperAdmin],
            'children' => $children
        ];

        $this->itemIsActive = false;
        $this->checkMenu($current_url);

        if($this->itemIsActive == false)
        {
            $this->checkMenu($second_current_url);
        }
    }

    private function checkMenu($current_url)
    {
        foreach($this->app['menu'] as $key => $val)
        {
            $active = false;

            foreach($val['children'] as $key_child => $val_child)
            {
                $this->app['menu'][$key]['children'][$key_child]['active'] = $current_url == $val_child['route'] ? true : false;

                if($this->app['menu'][$key]['children'][$key_child]['active'] == true)
                {
                    $this->checkAcl($this->app['menu'][$key]['children'][$key_child]['acl']);

                    $active = true;
                    $this->itemIsActive = true;
                }
            }

            if($current_url == $val['route'])
            {
                $active = true;
                $itemIsActive = true;
            }

            if($active == true)
            {
                $this->checkAcl($this->app['menu'][$key]['acl']);
            }

            $this->app['menu'][$key]['active'] = $active;
        }
    }

    protected function initFilter($name, $default, $route, $filter, $array)
    {
        $filters = session('filters');
        $selected = isset($filters[$this->prefix.'_'.$filter]) ? $filters[$this->prefix.'_'.$filter] : '';

        $this->app['filters'][$filter] =
        [
            'name' => $name,
            'default' => $default,
            'items' => $array,
            'route' => $route,
            'selected' => $selected,
        ];
    }

    private function initSearch()
    {
        $filters = session('filters');

        if(!empty($filters)) {
            $this->app['search'] = $this->search;
            $this->app['searchString'] = array_key_exists($this->prefix . '_search', $filters) ? $filters[$this->prefix . '_search'] : '';
        }
    }

    protected function setFilter($request, $filters)
    {
        $input = $request->all();

        foreach($filters as $filter)
        {
            if($request->exists($filter))
            {
                $filters = session('filters');
                $filters[$this->prefix.'_'.$filter] = $input[$filter];

                session(['filters' => $filters]);
            }
        }
    }

    protected function getFilter($filter = '')
    {
        if(!empty($filter))
        {
            $filters = session('filters');
            $filters = empty($filters) ? [] : $filters;

            return key_exists($this->prefix.'_'.$filter, $filters) ? $filters[$this->prefix.'_'.$filter] : '';
        }
    }

    private function initMetaData()
    {
        $this->app['meta']['title'] = 'ARTEHCMS |';

        foreach($this->app['menu'] as $key => $val)
        {
            if($val['active'] == true)
            {
                $this->app['meta']['title'] .= ' '.$val['name'];
                $this->app['current']['title'] = $val['name'];
            }

            foreach($val['children'] as $key_child => $val_child)
            {
                if($val_child['active'] == true)
                {
                    $this->app['meta']['title'] .= ' '.$val_child['name'];
                    $this->app['current']['title'] = $val_child['name'];
                }
            }
        }
    }

    private function initBreadcrumbData()
    {
        foreach($this->app['menu'] as $key => $val)
        {
            if($val['dashboard'] == true)
            {
                $this->app['breadcrumbs'][] =
                [
                    'name' => $val['name'],
                    'route' => $val['route'],
                    'active' => $val['active'],
                ];
            }
        }

        foreach($this->app['menu'] as $key => $val)
        {
            if($val['dashboard'] == false)
            {
                foreach($val['children'] as $key_child => $val_child)
                {
                    if($val_child['active'] == true)
                    {
                        $child =
                        [
                            'name' => $val_child['name'],
                            'route' => $val_child['route'],
                            'active' => !empty(request()->segment(4)) ? false : true,
                        ];
                    }
                }

                if($val['active'] == true)
                {
                    $this->app['breadcrumbs'][] =
                    [
                        'name' => $val['name'],
                        'route' => $val['route'],
                        'active' => !empty($child) ? false : true,
                    ];

                    if(!empty($child))
                    {
                        $this->app['breadcrumbs'][] = $child;
                    }
                }
            }
        }
    }

    protected function validatorMessages($fields = [])
    {
        $messages = [];

        if(!empty($fields))
        {
            if(in_array('email.required', $fields))
            {
                $messages['email.required'] = 'Puste pole: Adres e-mail';
            }

            if(in_array('email.email', $fields))
            {
                $messages['email.email'] = 'Błędne pole: Adres e-mail';
            }

            if(in_array('email.unique', $fields))
            {
                $messages['email.unique'] = 'Podany adres e-mail jeż już używany';
            }

            if(in_array('name.required', $fields))
            {
                $messages['name.required'] = 'Puste pole: Nazwa';
            }

            if(in_array('title.required', $fields))
            {
                $messages['title.required'] = 'Puste pole: Tytuł';
            }

            if(in_array('symbol.required', $fields))
            {
                $messages['symbol.required'] = 'Puste pole: Symbol';
            }

            if(in_array('password.required', $fields))
            {
                $messages['password.required'] = 'Puste pole: Hasło';
            }

            if(in_array('password.min', $fields))
            {
                $messages['password.min'] = 'Pole: Hasło musi mieć minimum :min znaków';
            }

            if(in_array('languages_id.required', $fields))
            {
                $messages['languages_id.required'] = 'Puste pole: Język';
            }

            if(in_array('menus_id.required', $fields))
            {
                $messages['menus_id.required'] = 'Puste pole: Menu';
            }

            if(in_array('modules_id.required', $fields))
            {
                $messages['modules_id.required'] = 'Puste pole: Moduł';
            }

            if(in_array('url.required', $fields))
            {
                $messages['url.required'] = 'Puste pole: Url';
            }

            if(in_array('url.unique', $fields))
            {
                $messages['url.unique'] = 'Podany url jest już używany';
            }
        }

        return $messages;
    }

    protected function checkUnique($input, $field, $table)
    {
        $validator = Validator::make($input,
        [
            $field => [Rule::unique($table, $field)->ignore($input['id'])],
        ]);

        return $validator->fails() ? false : true;
    }

    protected function createSlug($string)
    {
        return str_slug($string, '-');
    }

    protected function moduleSeoData(&$edit, $prefix)
    {
        $modules_id = Modules::getModuleIdBySymbol($prefix);

        $seo = Urls::getRowByModuleItemId($modules_id, $edit->id);

        $edit['url'] = !empty($seo['url']) ? $seo['url'] : '';
        $edit['meta_title'] = !empty($seo['meta_title']) ? $seo['meta_title'] : '';
        $edit['meta_description'] = !empty($seo['meta_description']) ? $seo['meta_description'] : '';
        $edit['meta_key_words'] = !empty($seo['meta_key_words']) ? $seo['meta_key_words'] : '';
    }

    protected function SaveImage(Request $request, $field = 'image')
    {
        $image = '';

        if (!empty($request[$field]))
        {
            if (!File::exists($this->uploadThumbsDir))
            {
                File::makeDirectory($this->uploadThumbsDir, 0755, true);
            }

            $imageRequest = $request->file($field);
            $image = time() . '.' .$field. '.' . $imageRequest->getClientOriginalExtension();
            $imageRequest->move($this->uploadSourceDir, $image);
        }

        return $image;
    }

    protected function DeleteImage($object, $field = 'image')
    {
        if (!empty($object->$field))
        {
            if (File::exists($this->uploadSourceDir . '/' . $object->$field))
            {
                File::delete($this->uploadSourceDir . '/' . $object->$field);
            }

            if (File::exists($this->uploadThumbsDir . '/' . $object->$field))
            {
                File::delete($this->uploadThumbsDir . '/' . $object->$field);
            }
        }
    }

    public function filters(Request $request)
    {
        $this->setFilter($request, $this->filters);

        return redirect($this->app['buttons']['index']);
    }

    public function search(Request $request)
    {
        $this->setFilter($request, ['search']);

        return redirect($this->app['buttons']['index']);
    }
}
