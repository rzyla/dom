<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class FrontController extends BaseController
{
    protected $app = [];
    protected $acl = [];
    protected $filters = [];
    protected $search = false;
    protected $prefix;
    protected $uploadSourceDir;
    protected $uploadThumbsDir;

    protected $paginationSizes =
        [
            5 => '5 wyników',
            10 => '10 wyników',
            20 => '20 wyników',
            50 => '50 wyników',
            100 => '100 wyników'
        ];

    function __construct($prefix = null)
    {
        if(!empty($prefix))
        {
            $this->prefix = $prefix;

            $this->uploadSourceDir = public_path('uploads/') . $prefix;
            $this->uploadThumbsDir = public_path('thumbs/uploads/') . $prefix;

            $this->app['current']['uploadSourceDir'] = 'uploads/' . $prefix . '/';
        }
    }

    protected function init() {}
}
