<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\AdminController;
use App\Notes;
use App\NotesGroups;

class NotesController extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth:admin');

        parent::__construct('notes');
    }

    public function index()
    {
        $this->init();

        $app = $this->app;
        $grid = Notes::select('notes.id', 'notes.title', 'notes_groups.name as notes_groups_name')
                    ->join('notes_groups', 'notes.notes_groups_id', '=', 'notes_groups.id')
                    ->paginate(10);

        return view('admin.notes.index', compact('app', 'grid'));
    }

    public function create(Request $request)
    {
        $this->init();

        $app = $this->app;
        $app['current']['title'] = 'Dodaj nową notatkę';

        $notesGroups = NotesGroups::orderBy('name')->get()->pluck('name', 'id')->all();

        return view('admin.notes.create', compact('app', 'notesGroups'));
    }

    public function store(Request $request)
    {
        $messages = $this->validatorMessages(
        [
            'title.required', 
        ]);

        $validator = Validator::make($request->all(), 
        [
            'title' => 'required',
        ], $messages);

        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $input = $request->all();
        $insert = Notes::create($input);
        
        return redirect()->route('admin.notes.index', ['id' => $insert->id])->with('success','Wpis dodany pomyślnie');
    }

    public function edit($id)
    {
        $this->init();

        $app = $this->app;
        $app['current']['title'] = 'Edytuj notatkę';

        $edit = Notes::find($id);

        $notesGroups = NotesGroups::orderBy('name')->get()->pluck('name', 'id')->all();

        return view('admin.notes.edit', compact('app', 'edit', 'notesGroups'));
    }

    public function update($id, Request $request)
    {
        $messages = $this->validatorMessages(
        [
            'title.required', 
        ]);

        $validator = Validator::make($request->all(), 
        [
            'title' => 'required',
        ], $messages);

        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();

        $update = Notes::find($id);
        $update->update($input);

        return redirect()->route('admin.notes.edit', ['id' => $update->id])->with('success','Wpis zaktualizowany pomyślnie');
    }

    public function destroy($id)
    {
        $delete = Notes::find($id);
        $delete->delete();

        return redirect()->route('admin.notes.index')->with('success', 'Wpis został usunięty');
    }
}
