<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\AdminController;
use App\Modules;
use App\Pages;
use App\Urls;

class PagesController extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth:admin');

        parent::__construct('pages', false);
    }

    public function index()
    {
        $this->init();

        $app = $this->app;
        $grid = Pages::paginate(10);

        return view('admin.pages.index', compact('app', 'grid'));
    }

    public function create(Request $request)
    {

    }

    public function store(Request $request)
    {

    }

    public function edit($id)
    {
        $this->init();

        $app = $this->app;
        $edit = Pages::find($id);

        $this->moduleSeoData($edit, $this->prefix);

        return view('admin.pages.edit', compact('app', 'edit'));
    }

    public function update($id, Request $request)
    {
        $pages = Pages::find($id);
        $module_id = Modules::getModuleIdBySymbol($this->prefix);

        $messages = $this->validatorMessages(
        [
            'title.required', 
            'url.required',
            'url.unique',
        ]);
    
        $validator = Validator::make($request->all(), 
        [
            'title' => 'required',
            'url' => ['required', Rule::unique('urls', 'url')->ignore(Urls::getIdByModuleItemId($module_id, $pages->id))],
        ], $messages);

        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $pages->update($input);

        Urls::updateUrlMetaByModuleItemId($input, $module_id, $pages->id);

        return redirect()->route('admin.pages.edit', ['id' => $pages->id])->with('success','Wpis zaktualizowany pomyślnie');
    }

    public function destroy($id)
    {
        //TODO: Weryfikacja

        if(true == true)
        {
            $pages = Pages::find($id);
            $pages->delete();

            return redirect()->route('admin.pages.index')->with('success', 'Wpis został usunięty');
        }
        else
        {
            return redirect()->route('admin.pages.index')->with('error', 'Istnieją wpisy powiązane z tym językiem.');
        }
    }
}
