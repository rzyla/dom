<?php

namespace App\Http\Controllers\Admin;

use App\BoardGames;
use App\BoardGamesPlays;
use App\BoardGamesPlaysScores;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BoardGamesController extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->filters = ['show', 'addons'];

        parent::__construct('board_games', ['create', 'filters']);
    }

    private function initController()
    {
        if($this->getFilter('show') == '')
        {
            $filters = session('filters');
            $filters[$this->prefix.'_show'] = 'boardgames';

            session(['filters' => $filters]);
        }

        if($this->getFilter('addons') == '')
        {
            $filters = session('filters');
            $filters[$this->prefix.'_addons'] = 'boardgames';

            session(['filters' => $filters]);
        }
        
        $this->show = ['all' => 'Pokaż wszystko', 'boardgames' => 'Tylko posiadane gry', 'addons' => 'Tylko lista życzeń'];
        $this->initFilter('', 'Pokaż wszystko', $this->app['buttons']['filters'], 'show', $this->show);

        $this->addons = ['all' => 'Pokaż wszystko', 'boardgames' => 'Tylko gry', 'addons' => 'Tylko dodatki'];
        $this->initFilter('', 'Pokaż wszystko', $this->app['buttons']['filters'], 'addons', $this->addons);
    }

    public function index()
    {
        $this->init();
        $this->initController();

        $app = $this->app;

        $board_games = [];
        if($this->getFilter('show') == 'all' || $this->getFilter('show') == 'boardgames')
        {
            $games = BoardGames::select('board_games.id', 'board_games.name', 'board_games.image', DB::raw('COUNT(board_games_plays.board_game_id) as plays'))
                ->leftjoin('board_games_plays', 'board_games_plays.board_game_id', '=', 'board_games.id');

            if($this->getFilter('addons') == 'boardgames')
            {
                $games->where('board_games.board_game_id', '=', 0);
            }

            if($this->getFilter('addons') == 'addons')
            {
                $games->where('board_games.board_game_id', '!=', 0);
            }

            $games->where('wishlist', '=', 0)
                ->groupBy('board_games.id', 'board_games.name', 'board_games.image')
                ->orderBy('name', 'asc');

            $board_games = $games->get();
        }

        $wish_list = [];
        if($this->getFilter('show') == 'all' || $this->getFilter('show') == 'addons')
        {
            $list =  BoardGames::select('*');

            if($this->getFilter('addons') == 'boardgames')
            {
                $list->where('board_game_id', '=', 0);
            }

            if($this->getFilter('addons') == 'addons')
            {
                $list->where('board_game_id', '!=', 0);
            }

            $wish_list = $list->where('wishlist', '=', 1)->orderBy('name', 'asc')->get();
        }

        $stats['board_games'] =
            [
                'all' => BoardGames::where('wishlist', '=', 0)->count(),
                'games' => BoardGames::where('wishlist', '=', 0)->where('board_game_id', '=', 0)->count(),
                'addons' => BoardGames::where('wishlist', '=', 0)->where('board_game_id', '>', 0)->count()
            ];
        $stats['wish_list'] =
            [
                'all' => BoardGames::where('wishlist', '=', 1)->count(),
                'games' => BoardGames::where('wishlist', '=', 1)->where('board_game_id', '=', 0)->count(),
                'addons' => BoardGames::where('wishlist', '=', 1)->where('board_game_id', '>', 0)->count()
            ];

        return view('admin.board_games.index', compact('app', 'board_games', 'wish_list', 'stats'));
    }

    public function create(Request $request)
    {
        $this->init();

        $app = $this->app;
        $app['current']['title'] = 'Dodaj nowego gracza';

        $board_games = BoardGames::orderBy('name')->get()->pluck('name', 'id')->all();

        return view('admin.board_games.create', compact('app', 'board_games'));
    }

    public function store(Request $request)
    {
        $this->init();

        $messages = $this->validatorMessages(
            [
                'name.required',
            ]);

        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
            ], $messages);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $input['wishlist'] = empty($input['wishlist']) ? 0 : 1;
        $input['extension'] = empty($input['board_game_id']) ? 0 : 1;

        if (!empty($input['image']))
        {
            $input['image'] = $this->SaveImage($request, 'image');
        }

        $insert = BoardGames::create($input);

        return redirect()->route('admin.board_games.index', ['id' => $insert->id])->with('success','Wpis dodany pomyślnie');
    }

    public function edit($id)
    {
        $this->init();

        $app = $this->app;
        $app['current']['title'] = 'Edytuj produkt';

        $edit = BoardGames::find($id);

        $board_games = BoardGames::where('id', '!=', $edit->id)->orderBy('name')->get()->pluck('name', 'id')->all();

        return view('admin.board_games.edit', compact('app', 'edit', 'board_games'));
    }

    public function update($id, Request $request)
    {
        $this->init();

        $messages = $this->validatorMessages(
            [
                'name.required',
            ]);

        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
            ], $messages);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $input['wishlist'] = empty($input['wishlist']) ? 0 : 1;
        $input['extension'] = empty($input['board_game_id']) ? 0 : 1;

        $update = BoardGames::find($id);

        if (!empty($input['deleteImage']))
        {
            $this->DeleteImage($update, 'image');
        }

        if (!empty($input['image']) || !empty($input['deleteImage']))
        {
            $input['image'] = $this->SaveImage($request, 'image');
        }

        $update->update($input);

        return redirect()->route('admin.board_games.edit', ['id' => $update->id])->with('success','Wpis zaktualizowany pomyślnie');
    }

    public function destroy($id)
    {
        $delete = BoardGames::find($id);
        $this->DeleteImage($delete, 'image');
        $delete->delete();

        $plays = BoardGamesPlays::where('board_game_id', '=', $id)->get()->pluck('id', 'id')->all();

        BoardGamesPlaysScores::whereIn('id', $plays)->delete();
        BoardGamesPlays::where('board_game_id', '=', $id)->delete();

        return redirect()->route('admin.board_games.index')->with('success', 'Wpis został usunięty');
    }
}
