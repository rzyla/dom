<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\AdminController;
use App\UsersAdmins;

class UsersAdminsController extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth:admin');

        parent::__construct('admins');
    }

    private function acl()
    {
        if($this->app['user']['acl'] <= parent::aclAdmin)
        {
            unset($this->acl[parent::aclSuperAdmin]);
        }

        if($this->app['user']['acl'] <= parent::aclUser)
        {
            unset($this->acl[parent::aclAdmin]);
        }

        if($this->app['user']['acl'] <= parent::aclGuest)
        {
            unset($this->acl[parent::aclUser]);
        }
    }

    public function index()
    {
        $this->init();
        $this->acl();

        $app = $this->app;
        $acl = $this->acl;

        if($this->app['user']['acl'] == 9)
        {
            $grid = UsersAdmins::paginate(10);
        }
        else
        {
            $grid = UsersAdmins::where('acl', '!=', 9)->paginate(10);
        }

        return view('admin.admins.index', compact('app', 'grid', 'acl'));
    }

    public function create(Request $request)
    {
        $this->init();
        $this->acl();

        $app = $this->app;
        $acl = $this->acl;

        return view('admin.admins.create', compact('app', 'grid', 'acl'));
    }

    public function store(Request $request)
    {
        $messages = $this->validatorMessages(
        [
            'name.required', 
            'email.required',
            'email.email',
            'email.unique',
            'password.required',
            'password.min',
        ]);

        $validator = Validator::make($request->all(), 
        [
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users_admins')],
            'password' => 'required|min:6',
        ], $messages);

        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput($request->except(['password']));
        }
        
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        
        $admins = UsersAdmins::create($input);
        
        return redirect()->route('admin.admins.edit', ['id' => $admins->id])->with('success','Wpis dodany pomyślnie');
    }

    public function edit($id)
    {
        $this->init();
        $this->acl();

        $app = $this->app;
        $acl = $this->acl;
        $edit = UsersAdmins::find($id);

        return view('admin.admins.edit', compact('app', 'edit', 'acl'));
    }

    public function update($id, Request $request)
    {
        $messages = $this->validatorMessages(
        [
            'name.required', 
            'email.required',
            'email.email',
            'email.unique',
            'password.required',
            'password.min',
        ]);

        $validator = Validator::make($request->all(), 
        [
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users_admins')->ignore($id)],
        ], $messages);

        $validator->sometimes('password', 'required|min:6', function ($input) 
        {
            return !empty( $input->password);
        });

        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput($request->except(['password']));
        }

        $input = $request->all();

        if(!empty($input['password']))
        {
            $input['password'] = Hash::make($input['password']);
        }
        else
        {
            unset($input['password']);
        }

        $admins = UsersAdmins::find($id);
        $admins->update($input);

        return redirect()->route('admin.admins.edit', ['id' => $admins->id])->with('success','Wpis zaktualizowany pomyślnie');
    }

    public function destroy($id)
    {
        $admin = UsersAdmins::find($id);
        $admin->delete();
        
        return redirect()->route('admin.admins.index')->with('success', 'Wpis został usunięty');
    }
}
