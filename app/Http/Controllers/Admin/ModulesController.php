<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\AdminController;
use App\Modules;

class ModulesController extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth:admin');

        parent::__construct('modules');
    }

    public function index()
    {
        $this->init();

        $app = $this->app;
        $grid = Modules::paginate(10);

        return view('admin.modules.index', compact('app', 'grid'));
    }

    public function create(Request $request)
    {
        $this->init();
        $app = $this->app;

        return view('admin.modules.create', compact('app'));
    }

    public function store(Request $request)
    {
        $messages = $this->validatorMessages(
        [
            'name.required', 
            'symbol.required',
        ]);

        $validator = Validator::make($request->all(), 
        [
            'name' => 'required',
            'symbol' => 'required',
        ], $messages);

        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $input = $request->all();
        $languages = Modules::create($input);
        
        return redirect()->route('admin.modules.edit', ['id' => $languages->id])->with('success','Wpis dodany pomyślnie');
    }

    public function edit($id)
    {
        $this->init();

        $app = $this->app;
        $edit = Modules::find($id);

        return view('admin.modules.edit', compact('app', 'edit'));
    }

    public function update($id, Request $request)
    {
        $messages = $this->validatorMessages(
        [
            'name.required', 
            'symbol.required',
        ]);

        $validator = Validator::make($request->all(), 
        [
            'name' => 'required',
            'symbol' => 'required',
        ], $messages);

        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();

        $modules = Modules::find($id);
        $modules->update($input);

        return redirect()->route('admin.modules.edit', ['id' => $modules->id])->with('success','Wpis zaktualizowany pomyślnie');
    }

    public function destroy($id)
    {
        //TODO: Weryfikacja

        if(true == true)
        {
            $modules = Modules::find($id);
            $modules->delete();

            return redirect()->route('admin.modules.index')->with('success', 'Wpis został usunięty');
        }
        else
        {
            return redirect()->route('admin.modules.index')->with('error', 'Istnieją wpisy powiązane z tym menu.');
        }
    }
}
