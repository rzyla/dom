<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\AdminController;
use App\ShoppingLists;
use App\ShoppingListsShops;
use App\ShopingListsPositions;
use App\ShoppingListsShopsShoppingLists;
use mysql_xdevapi\Exception;

class ShoppingListsController extends AdminController
{
    private $shops = [];

    public function __construct()
    {
        $this->middleware('auth:admin');

        parent::__construct('shopping_lists', ['create', 'filters', 'search']);

        $this->filters = ['shops', 'prio', 'arrows'];
        $this->search = true;
    }

    private function initController()
    {
        $this->shops = ShoppingListsShops::orderBy('name')->get()->pluck('name', 'id')->all();
        $this->initFilter('', 'Wszystkie sklepy', $this->app['buttons']['filters'], 'shops', $this->shops);
        $this->initFilter('', 'Wszystkie produkty', $this->app['buttons']['filters'], 'prio', ['1' => 'Tylko produkty ważne']);
        $this->initFilter('', 'Aktualizuj kolejność', $this->app['buttons']['filters'], 'arrows', ['1' => 'Tak']);
    }

    public function index()
    {
        $this->init();
        $this->initController();

        $app = $this->app;
        $app['arrows'] = !empty($this->app['filters']['shops']['selected']) && !empty($this->app['filters']['arrows']['selected']);

        $query = ShoppingLists::select('shopping_lists.*');

        if(!empty($this->getFilter('shops')))
        {
            $query->join('shopping_lists_shops_lists', 'shopping_lists_shops_lists.shopping_lists_id', '=', 'shopping_lists.id')
                ->where('shopping_lists_shops_lists.shopping_lists_shops_id', '=', $this->getFilter('shops'));
        }

        if(!empty($this->getFilter('search')))
        {
            $query->where('name', 'like', '%' . $this->getFilter('search') . '%');
        }

        if(!empty($this->getFilter('prio')))
        {
            $query->where('prio', '=', '1');
        }

        $query->leftJoin('shopping_lists_positions', function ($join) {

            $shop_id = !empty($this->app['filters']['shops']['selected']) ? $this->app['filters']['shops']['selected'] : 0;

            $join->on('shopping_lists_positions.shopping_lists_id', '=', 'shopping_lists.id')
                ->where('shopping_lists_positions.shopping_lists_shops_id', '=', $shop_id);
        });

        $query->orderBy('shopping_lists_positions.position', 'asc');

        $grid = $query->paginate(100);

        foreach($grid as $key => $val)
        {
            $shops = ShoppingListsShops::select('*')
                ->join('shopping_lists_shops_lists', 'shopping_lists_shops_lists.shopping_lists_shops_id', '=', 'shopping_lists_shops.id')
                ->where('shopping_lists_shops_lists.shopping_lists_id', '=', $val['id'])
                ->get()
                ->pluck('name');

            $grid[$key]['shops'] = !empty($shops) ? implode(', ', $shops->toArray()) : '';
        }

        return view('admin.shopping_lists.index', compact('app', 'grid'));
    }

    public function create(Request $request)
    {
        $this->init();

        $app = $this->app;
        $app['current']['title'] = 'Dodaj nowy produkt';

        $shoppingListsShops = ShoppingListsShops::orderBy('name')->get()->pluck('name', 'id')->all();

        return view('admin.shopping_lists.create', compact('app', 'shoppingListsShops'));
    }

    public function store(Request $request)
    {
        $this->init();
        $this->initController();

        $messages = $this->validatorMessages(
        [
            'name.required',
        ]);

        $validator = Validator::make($request->all(),
        [
            'name' => 'required',
        ], $messages);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $input['prio'] = empty($input['prio']) ? 0 : 1;

        if (!empty($input['image']))
        {
            $input['image'] = $this->SaveImage($request, 'image');
        }

        $insert = ShoppingLists::create($input);

        $this->addProductsToStore($input, $insert->id);

        return redirect()->route('admin.shopping_lists.index', ['id' => $insert->id])->with('success','Wpis dodany pomyślnie');
    }

    public function edit($id)
    {
        $this->init();

        $app = $this->app;
        $app['current']['title'] = 'Edytuj produkt';

        $edit = ShoppingLists::find($id);

        $shoppingListsShopsShoppingLists = ShoppingListsShopsShoppingLists::where('shopping_lists_id', '=', $id)->get()->pluck('shopping_lists_id', 'shopping_lists_shops_id')->all();
        $shoppingListsShops = ShoppingListsShops::orderBy('name')->get()->pluck('name', 'id')->all();

        return view('admin.shopping_lists.edit', compact('app', 'edit', 'shoppingListsShops', 'shoppingListsShopsShoppingLists'));
    }

    public function update($id, Request $request)
    {
        $this->init();
        $this->initController();

        $messages = $this->validatorMessages(
        [
            'name.required',
        ]);

        $validator = Validator::make($request->all(),
        [
            'name' => 'required',
        ], $messages);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $input['prio'] = empty($input['prio']) ? 0 : 1;

        $update = ShoppingLists::find($id);

        if (!empty($input['deleteImage']))
        {
            $this->DeleteImage($update, 'image');
        }

        if (!empty($input['image']) || !empty($input['deleteImage']))
        {
            $input['image'] = $this->SaveImage($request, 'image');
        }

        $update->update($input);

        $this->addProductsToStore($input, $update->id);

        return redirect()->route('admin.shopping_lists.edit', ['id' => $update->id])->with('success','Wpis zaktualizowany pomyślnie');
    }

    public function destroy($id)
    {
        $delete = ShoppingLists::find($id);
        $this->DeleteImage($delete, 'image');
        $delete->delete();

        ShoppingListsShopsShoppingLists::where('shopping_lists_id', '=', $id)->delete();

        return redirect()->route('admin.shopping_lists.index')->with('success', 'Wpis został usunięty');
    }

    private function addProductsToStore($input = [], $product_id = 0)
    {
        if(!empty($input['shops']) && !empty($product_id))
        {
            ShoppingListsShopsShoppingLists
                ::where('shopping_lists_id', '=', $product_id)
                ->delete();

            ShopingListsPositions
                ::where('shopping_lists_id', '=', $product_id)
                ->delete();

            foreach($input['shops'] as $shop)
            {
                ShoppingListsShopsShoppingLists
                    ::create(['shopping_lists_shops_id' => $shop, 'shopping_lists_id' => $product_id]);

                $max = ShopingListsPositions
                    ::where('shopping_lists_shops_id', '=', $shop)
                    ->max('position');

                ShopingListsPositions::create(
                    [
                        'shopping_lists_id' => $product_id,
                        'shopping_lists_shops_id' => $shop,
                        'position' => $max + 1
                    ]);
            }
        }
    }

    public function upDown($action, $id)
    {
        $this->init();
        $this->initController();

        $shop_id = !empty($this->app['filters']['shops']['selected']) ? $this->app['filters']['shops']['selected'] : 0;

        if(!empty($action) && !empty($id) && !empty($shop_id))
        {
            switch($action)
            {
                case "top":
                    ShopingListsPositions
                        ::where('shopping_lists_id', '=', $id)
                        ->where('shopping_lists_shops_id', '=', $shop_id)
                        ->delete();

                    $min = ShopingListsPositions
                        ::where('shopping_lists_shops_id', '=', $shop_id)
                        ->min('position');

                    ShopingListsPositions::create(
                        [
                            'shopping_lists_id' => $id,
                            'shopping_lists_shops_id' => $shop_id,
                            'position' => $min - 1
                        ]);
                    break;
                case "bottom":
                    ShopingListsPositions
                        ::where('shopping_lists_id', '=', $id)
                        ->where('shopping_lists_shops_id', '=', $shop_id)
                        ->delete();

                    $max = ShopingListsPositions
                        ::where('shopping_lists_shops_id', '=', $shop_id)
                        ->max('position');

                    ShopingListsPositions::create(
                        [
                            'shopping_lists_id' => $id,
                            'shopping_lists_shops_id' => $shop_id,
                            'position' => $max + 1
                        ]);
                    break;
                case "up":
                    $position = ShopingListsPositions
                        ::where('shopping_lists_id', '=', $id)
                        ->where('shopping_lists_shops_id', '=', $shop_id)
                        ->first();

                    $positionUp = ShopingListsPositions
                        ::where('shopping_lists_shops_id', '=', $shop_id)
                        ->where('position', '<', $position->position)
                        ->orderBy('shopping_lists_positions.position', 'desc')
                        ->first();

                    ShopingListsPositions
                        ::where('shopping_lists_id', '=', $id)
                        ->where('shopping_lists_shops_id', '=', $shop_id)
                        ->update(['position' => $positionUp->position]);

                    ShopingListsPositions
                        ::where('shopping_lists_id', '=', $positionUp->shopping_lists_id)
                        ->where('shopping_lists_shops_id', '=', $shop_id)
                        ->update(['position' => $position->position]);
                    break;
                case "down":
                    $position = ShopingListsPositions
                        ::where('shopping_lists_id', '=', $id)
                        ->where('shopping_lists_shops_id', '=', $shop_id)
                        ->first();

                    $positionDown = ShopingListsPositions
                        ::where('shopping_lists_shops_id', '=', $shop_id)
                        ->where('position', '>', $position->position)
                        ->orderBy('shopping_lists_positions.position', 'asc')
                        ->first();

                    ShopingListsPositions
                        ::where('shopping_lists_id', '=', $id)
                        ->where('shopping_lists_shops_id', '=', $shop_id)
                        ->update(['position' => $positionDown->position]);

                    ShopingListsPositions
                        ::where('shopping_lists_id', '=', $positionDown->shopping_lists_id)
                        ->where('shopping_lists_shops_id', '=', $shop_id)
                        ->update(['position' => $position->position]);
                    break;
            }
        }

        return redirect()->route('admin.shopping_lists.index');
    }

    public function ajaxQuantityPriority(Request $request)
    {
        try
        {
            $input = $request->all();
            $update = ShoppingLists::find($input['id']);
            $update->update($input);

            echo 'OK';
        } catch (Exception $ex) {}
    }
}
