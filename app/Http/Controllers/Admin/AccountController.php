<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\UsersAdmins;

class AccountController extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth:admin');

        parent::__construct('account');
    }

    private function initAccount($id)
    {
        if($this->app['user']['id'] != $id)
        {
            $this->redirectDashboard();
        }
    }

    public function index()
    {
        $this->redirectDashboard();
    }

    public function edit($id)
    {
        $this->init();
        $this->initAccount($id);
        
        $app = $this->app;
        $edit = UsersAdmins::find($id);

        return view('admin.account.edit', compact('app', 'edit'));
    }

    public function update($id, Request $request)
    {
        $this->init();
        $this->initAccount($id);

        $messages = $this->validatorMessages(
        [
            'name.required', 
            'email.required',
            'email.email',
            'email.unique',
            'password.required',
            'password.min',
        ]);
    
        $validator = Validator::make($request->all(), 
        [
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users_admins')->ignore($id)],
        ], $messages);
    
        $validator->sometimes('password', 'required|min:6', function ($input) 
        {
            return !empty( $input->password);
        });
    
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput($request->except(['password']));
        }
    
        $input = $request->all();
        $admins = UsersAdmins::find($id);
    
        if(!empty($input['password']))
        {
            $input['password'] = Hash::make($input['password']);
        }
        else
        {
            unset($input['password']);
        }

        if (!empty($input['deleteAvatar']))
        {
            $this->DeleteImage($admins, 'avatar');
        }
        
        if (!empty($input['avatar']) || !empty($input['deleteAvatar']))
        {
            $input['avatar'] = $this->SaveImage($request, 'avatar');
        }

        $admins->update($input);
    
        return redirect()->route('admin.account.edit', ['id' => $admins->id])->with('success','Wpis zaktualizowany pomyślnie');
    }
}
