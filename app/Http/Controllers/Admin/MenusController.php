<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\AdminController;
use App\Menus;

class MenusController extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth:admin');

        parent::__construct('menus');
    }

    public function index()
    {
        $this->init();

        $app = $this->app;
        $grid = Menus::paginate(10);

        return view('admin.menus.index', compact('app', 'grid'));
    }

    public function create(Request $request)
    {
        $this->init();
        $app = $this->app;

        return view('admin.menus.create', compact('app'));
    }

    public function store(Request $request)
    {
        $messages = $this->validatorMessages(
        [
            'name.required', 
            'symbol.required',
            'languages_id.required',
        ]);

        $validator = Validator::make($request->all(), 
        [
            'name' => 'required',
            'symbol' => 'required',
            'languages_id' => 'required',
        ], $messages);

        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $input = $request->all();
        $languages = Menus::create($input);
        
        return redirect()->route('admin.menus.edit', ['id' => $languages->id])->with('success','Wpis dodany pomyślnie');
    }

    public function edit($id)
    {
        $this->init();

        $app = $this->app;
        $edit = Menus::find($id);

        return view('admin.menus.edit', compact('app', 'edit'));
    }

    public function update($id, Request $request)
    {
        $messages = $this->validatorMessages(
        [
            'name.required', 
            'symbol.required',
            'languages_id.required',
        ]);

        $validator = Validator::make($request->all(), 
        [
            'name' => 'required',
            'symbol' => 'required',
            'languages_id' => 'required',
        ], $messages);

        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();

        $menus = Menus::find($id);
        $menus->update($input);

        return redirect()->route('admin.menus.edit', ['id' => $menus->id])->with('success','Wpis zaktualizowany pomyślnie');
    }

    public function destroy($id)
    {
        //TODO: Weryfikacja

        if(true == true)
        {
            $menus = Menus::find($id);
            $menus->delete();

            return redirect()->route('admin.menus.index')->with('success', 'Wpis został usunięty');
        }
        else
        {
            return redirect()->route('admin.menus.index')->with('error', 'Istnieją wpisy powiązane z tym menu.');
        }
    }

    public function ajaxGetMenus(Request $request)
    {
        return response()->json(empty($request->input('id')) ? null : Menus::getMenus($request->input('id'), $request->input('label'), true));
    } 
}
