<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\AdminController;
use App\Menus;
use App\Modules;
use App\Urls;

class UrlsController extends AdminController
{
    private $menus = [];
    private $modules = [];
    private $roots = [];
    private $isPagination;

    public function __construct()
    {
        $this->middleware('auth:admin');

        parent::__construct('urls');

        $this->filters = ['menus', 'modules'];
    }

    private function initUrls()
    {
        $filters = session('filters');

        //$this->menus = Menus::getMenus($this->app['languages_id']);
        $this->initFilter('Menu', 'Wszystkie menu', route('admin.urls.filters.submit'), 'menus', $this->menus);
        $this->modules = Modules::getModules(true);
        $this->initFilter('Moduł', 'Wszystkie moduły', route('admin.urls.filters.submit'), 'modules', Modules::getModules());
        $this->roots = Urls::getRoots();
    }

    public function filters(Request $request)
    {
        //$this->setFilter($request, $this->filters);

        //return redirect()->route('admin.urls.index');
    }

    public function index()
    {
        $this->init();
        $this->initUrls();

        $app = $this->app;
        $menus = $this->menus;
        $roots = $this->roots;
        //$langs = $this->app['languages'];
        $modules = Modules::getModules();
        $grid = $this->getGrid();
        $isPagination = $this->isPagination;

        return view('admin.urls.index', compact('app', 'menus', 'modules', 'roots', 'grid', 'langs', 'isPagination'));
    }

    public function create(Request $request)
    {
        $this->init();
        $this->initUrls();

        $app = $this->app;
        $menus = $this->menus;
        $roots = [0 => ' - Główny element'] + $this->roots;
        $modules = $this->modules;
        $filters = $this->app['filters'];
        $id = 0;

        return view('admin.urls.create', compact('app', 'menus', 'modules', 'roots', 'filters', 'id'));
    }

    public function store(Request $request)
    {
        $messages = $this->validatorMessages(
        [
            'name.required',
            'menus_id.required',
            'modules_id.required',
            'url.required',
            'url.unique',
        ]);

        $validator = Validator::make($request->all(),
        [
            'name' => 'required',
            'menus_id' => 'required',
            'modules_id' => 'required',
            'url' => ['required', Rule::unique('urls', 'url')],
        ], $messages);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();

        if(!empty($input['modules_id']))
        {
            $modules = Modules::find($input['modules_id']);

            if(!empty($modules->symbol))
            {
                $class = 'App\\' . $modules->class;

                $module = $class::create(['active' => 0]);
                $input['modules_items_id'] = $module->id;
            }
        }

        $urls = Urls::create($input);

        return redirect()->route('admin.urls.edit', ['id' => $urls->id])->with('success','Wpis dodany pomyślnie');
    }

    public function edit($id)
    {
        $this->init();
        $this->initUrls();

        $app = $this->app;
        $menus = $this->menus;
        $roots = [0 => ' - Główny element'] + $this->roots;
        $modules = $this->modules;
        $filters = $this->app['filters'];
        $edit = Urls::find($id);

        return view('admin.urls.edit', compact('app', 'menus', 'modules', 'roots', 'filters', 'id', 'edit'));
    }

    public function update($id, Request $request)
    {
        $messages = $this->validatorMessages(
        [
            'name.required',
            'menus_id.required',
            'modules_id.required',
            'url.required',
            'url.unique',
        ]);

        $validator = Validator::make($request->all(),
        [
            'name' => 'required',
            'menus_id' => 'required',
            'modules_id' => 'required',
            'url' => ['required', Rule::unique('urls', 'url')->ignore($id)],
        ], $messages);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();

        $urls = Urls::find($id);
        $urls->update($input);

        return redirect()->route('admin.urls.edit', ['id' => $urls->id])->with('success','Wpis zaktualizowany pomyślnie');
    }

    public function destroy($id)
    {
        //TODO: Weryfikacja

        if(true == true)
        {
            $urls = Urls::find($id);
            $urls->delete();

            return redirect()->route('admin.urls.index')->with('success', 'Wpis został usunięty');
        }
        else
        {
            return redirect()->route('admin.urls.index')->with('error', 'Istnieją wpisy powiązane z tym językiem.');
        }
    }

    public function ajaxCheckUrl(Request $request)
    {
        $input = $request->all();

        return response()->json($this->checkUnique(['url' => $input['url'], 'id' => $input['id']], 'url', 'urls'));
    }

    public function ajaxGenerateUrl(Request $request)
    {
        $input = $request->all();
        $string = $this->createSlug($input['string']);

        while(!$this->checkUnique(['url' => $string, 'id' => $input['id']], 'url', 'urls'))
        {
            $i++;
            $string = $this->createSlug($input['string']).$i;
        }

        return response()->json($string);
    }

    public function ajaxGetMenu(Request $request)
    {
        $input = $request->all();

        $id = !empty($input['id']) ? $input['id'] : 0;
        $without = !empty($input['without']) ? $input['without'] : 0;

        return response()->json(empty($id) ? null : Urls::getRoots($id, $without));
    }

    private function getGrid()
    {
        $array = [];

        foreach($this->menus as $menu_id => $menu)
        {
            $array[$menu_id] =
            [
                'type' => 'menu',
                'id' => $menu_id,
                'name' => $menu,
                'items' => Urls::getAdminGrid($this->app['languages_id'], $menu_id, 0),
            ];
        }

        return $array;
    }
}
