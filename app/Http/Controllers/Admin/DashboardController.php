<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use App\Notes;

class DashboardController extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $this->init();
        $app = $this->app;

        $notes = Notes::getDashboardNotes();

        return view('admin.dashboard.index', compact('app', 'notes'));
    }
}
