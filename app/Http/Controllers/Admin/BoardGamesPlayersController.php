<?php

namespace App\Http\Controllers\Admin;

use App\BoardGamesPlayers;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BoardGamesPlayersController  extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth:admin');

        parent::__construct('board_games_players', ['create']);
    }

    private function initController() {}

    public function index()
    {
        $this->init();
        $this->initController();

        $app = $this->app;

        $grid = BoardGamesPlayers::select('*')->orderBy('name', 'asc')->paginate(10);

        return view('admin.board_games_players.index', compact('app', 'grid'));
    }

    public function create(Request $request)
    {
        $this->init();

        $app = $this->app;
        $app['current']['title'] = 'Dodaj nowego gracza';

        return view('admin.board_games_players.create', compact('app'));
    }

    public function store(Request $request)
    {
        $messages = $this->validatorMessages(
            [
                'name.required',
            ]);

        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
            ], $messages);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $insert = BoardGamesPlayers::create($input);

        return redirect()->route('admin.board_games_players.index', ['id' => $insert->id])->with('success','Wpis dodany pomyślnie');
    }

    public function edit($id)
    {
        $this->init();

        $app = $this->app;
        $app['current']['title'] = 'Edytuj gracza';

        $edit = BoardGamesPlayers::find($id);

        return view('admin.board_games_players.edit', compact('app', 'edit'));
    }

    public function update($id, Request $request)
    {
        $messages = $this->validatorMessages(
            [
                'name.required',
            ]);

        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
            ], $messages);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();

        $update = BoardGamesPlayers::find($id);
        $update->update($input);

        return redirect()->route('admin.board_games_players.edit', ['id' => $update->id])->with('success','Wpis zaktualizowany pomyślnie');
    }

    public function destroy($id)
    {
        $delete = BoardGamesPlayers::find($id);
        $delete->delete();

        return redirect()->route('admin.board_games_players.index')->with('success', 'Wpis został usunięty');
    }
}
