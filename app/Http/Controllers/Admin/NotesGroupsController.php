<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\AdminController;
use App\Notes;
use App\NotesGroups;

class NotesGroupsController extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth:admin');

        parent::__construct('notes_groups');
    }

    public function index()
    {
        $this->init();

        $app = $this->app;
        $grid = NotesGroups::paginate(10);

        return view('admin.notes_groups.index', compact('app', 'grid'));
    }

    public function create(Request $request)
    {
        $this->init();
        $app = $this->app;

        $app['current']['title'] = 'Dodaj nową grupę notatek';

        return view('admin.notes_groups.create', compact('app'));
    }

    public function store(Request $request)
    {
        $messages = $this->validatorMessages(
        [
            'name.required', 
        ]);

        $validator = Validator::make($request->all(), 
        [
            'name' => 'required',
        ], $messages);

        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $input = $request->all();
        $insert = NotesGroups::create($input);
        
        return redirect()->route('admin.notes_groups.index', ['id' => $insert->id])->with('success','Wpis dodany pomyślnie');
    }

    public function edit($id)
    {
        $this->init();

        $app = $this->app;
        $app['current']['title'] = 'Edytuj grupę notatek';

        $edit = NotesGroups::find($id);

        return view('admin.notes_groups.edit', compact('app', 'edit'));
    }

    public function update($id, Request $request)
    {
        $messages = $this->validatorMessages(
        [
            'name.required', 
        ]);

        $validator = Validator::make($request->all(), 
        [
            'name' => 'required',
        ], $messages);

        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();

        $update = NotesGroups::find($id);
        $update->update($input);

        return redirect()->route('admin.notes_groups.edit', ['id' => $update->id])->with('success','Wpis zaktualizowany pomyślnie');
    }

    public function destroy($id)
    {
        $check = Notes::where('notes_groups_id', '=', $id)->count();

        if($check == 0)
        {
            $delete = NotesGroups::find($id);
            $delete->delete();

            return redirect()->route('admin.notes_groups.index')->with('success', 'Wpis został usunięty');
        }
        else
        {
            return redirect()->route('admin.notes_groups.index')->with('error', 'Istnieją wpisy powiązane z tym elementem.');
        }
    }
}
