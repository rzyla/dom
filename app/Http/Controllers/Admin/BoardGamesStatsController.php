<?php

namespace App\Http\Controllers\Admin;

use App\BoardGames;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BoardGamesStatsController  extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->filters = ['year'];

        parent::__construct('board_games_stats', ['filters']);
    }

    private function initController()
    {
        if($this->getFilter('year') == '')
        {
            $filters = session('filters');
            $filters[$this->prefix.'_year'] = date("Y");

            session(['filters' => $filters]);
        }

        $this->year =
            [
                'all' => 'Pokaż wszystko',
                date("Y") => date("Y"),
                date("Y") - 1 => date("Y") - 1,
                date("Y") - 2 => date("Y") - 2,
                date("Y") - 3 => date("Y") - 3,
                date("Y") - 4 => date("Y") - 4
            ];
        $this->initFilter('', 'Pokaż wszystko', $this->app['buttons']['filters'], 'year', $this->year);
    }

    public function index()
    {
        $this->init();
        $this->initController();

        $app = $this->app;

        $grid = BoardGames::select('board_games.id', 'board_games.name', 'board_games.image', DB::raw('COUNT(board_games_plays.board_game_id) as plays'))
                ->leftjoin('board_games_plays', function ($join)
                {
                    if($this->getFilter('year') != 'all') {
                        $join->on('board_games_plays.board_game_id', '=', 'board_games.id')
                            ->whereYear('board_games_plays.datetime', '=', $this->getFilter('year'));
                    }
                    else
                    {
                        $join->on('board_games_plays.board_game_id', '=', 'board_games.id');
                    }
                })
                ->where('board_games.board_game_id', '=', 0)
                ->where('wishlist', '=', 0)
                ->groupBy('board_games.id', 'board_games.name', 'board_games.image')
                ->orderBy('plays', 'desc')
                ->orderBy('board_games.name', 'asc')
                ->get();

        return view('admin.board_games_stats.index', compact('app', 'grid'));
    }
}
