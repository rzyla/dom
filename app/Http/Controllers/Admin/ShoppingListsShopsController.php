<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\AdminController;
use App\ShoppingLists;
use App\ShoppingListsShops;

class ShoppingListsShopsController extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth:admin');

        parent::__construct('shopping_lists_shops');
    }

    public function index()
    {
        $this->init();

        $app = $this->app;
        $grid = ShoppingListsShops::orderBy('name', 'asc')->paginate(10);

        return view('admin.shopping_lists_shops.index', compact('app', 'grid'));
    }

    public function create(Request $request)
    {
        $this->init();

        $app = $this->app;
        $app['current']['title'] = 'Dodaj nowy sklep';

        return view('admin.shopping_lists_shops.create', compact('app'));
    }

    public function store(Request $request)
    {
        $messages = $this->validatorMessages(
        [
            'name.required',
        ]);

        $validator = Validator::make($request->all(),
        [
            'name' => 'required',
        ], $messages);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $insert = ShoppingListsShops::create($input);

        return redirect()->route('admin.shopping_lists_shops.index', ['id' => $insert->id])->with('success','Wpis dodany pomyślnie');
    }

    public function edit($id)
    {
        $this->init();

        $app = $this->app;
        $app['current']['title'] = 'Edytuj sklep';

        $edit = ShoppingListsShops::find($id);

        return view('admin.shopping_lists_shops.edit', compact('app', 'edit'));
    }

    public function update($id, Request $request)
    {
        $messages = $this->validatorMessages(
        [
            'title.required',
        ]);

        $validator = Validator::make($request->all(),
        [
            'title' => 'required',
        ], $messages);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();

        $update = ShoppingListsShops::find($id);
        $update->update($input);

        return redirect()->route('admin.shopping_lists_shops.edit', ['id' => $update->id])->with('success','Wpis zaktualizowany pomyślnie');
    }

    public function destroy($id)
    {
        $delete = ShoppingListsShops::find($id);
        $delete->delete();
    }
}
