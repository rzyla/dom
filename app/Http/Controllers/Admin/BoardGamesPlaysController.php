<?php

namespace App\Http\Controllers\Admin;

use App\BoardGames;
use App\BoardGamesPlayers;
use App\BoardGamesPlays;
use App\BoardGamesPlaysScores;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Validator;

class BoardGamesPlaysController extends AdminController
{
    public function __construct()
    {
        $this->middleware('auth:admin');

        $this->filters = ['games'];

        parent::__construct('board_games_plays', ['create', 'filters']);
    }

    private function initController()
    {
        $this->games = BoardGames::where('board_game_id', '=', '0')->orderBy('name')->get()->pluck('name', 'id')->all();
        $this->initFilter('', 'Wszystkie gry', $this->app['buttons']['filters'], 'games', $this->games);
    }

    public function index()
    {
        $this->init();
        $this->initController();

        $app = $this->app;

        $grid = BoardGamesPlays::select('board_games_plays.*', 'board_games.name')
            ->join('board_games', 'board_games.id', '=', 'board_games_plays.board_game_id');

        if(!empty($this->getFilter('games')))
        {
            $grid->where('board_games_plays.board_game_id', '=', $this->getFilter('games'));
        }

        $grid = $grid->orderBy('board_games_plays.datetime', 'desc')->paginate(100);

        foreach($grid as $key => $val)
        {
            $e = explode(' ', $val['datetime']);
            $grid[$key]['date'] = $e[0];

            $wins = BoardGamesPlaysScores::select('board_games_players.name', 'board_games_plays_scores.score')
                ->join('board_games_players', 'board_games_players.id', '=', 'board_games_plays_scores.board_games_players_id')
                ->where('board_games_plays_id', '=', $val['id'])
                ->where('win', '=', 1)
                ->get();

            $w = [];

            foreach($wins as $win)
            {
                $w[] = !empty($win['score']) ? $win['name'].'('.$win['score'].')' : $win['name'];
            }

            $grid[$key]['wins'] = implode(', ', $w);
        }

        $stats = $this->stats();
        $filter_game = $this->getFilter('games');

        return view('admin.board_games_plays.index', compact('app', 'grid', 'stats', 'filter_game'));
    }

    public function create(Request $request)
    {
        $this->init();

        $app = $this->app;
        $gamesFilter = $this->getFilter('games');

        $app['current']['title'] = 'Dodaj nowy wynik';

        $board_games = BoardGames::where('extension','=','0')->orderBy('name')->get()->pluck('name', 'id')->all();
        $players = BoardGamesPlayers::orderBy('name')->get()->pluck('name', 'id')->all();

        return view('admin.board_games_plays.create', compact('app', 'board_games', 'players', 'gamesFilter'));
    }

    public function store(Request $request)
    {
        $this->init();

        $messages = $this->validatorMessages(
            [
                'datetime.required',
            ]);

        $validator = Validator::make($request->all(),
            [
                'datetime' => 'required',
            ], $messages);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $input['win'] = !empty($input['win']) ? 1 : 0;
        $insert = BoardGamesPlays::create($input);

        $this->saveScores($insert->id, $input);

        return redirect()->route('admin.board_games_plays.index', ['id' => $insert->id])->with('success','Wpis dodany pomyślnie');
    }

    public function edit($id)
    {
        $this->init();

        $app = $this->app;
        $app['current']['title'] = 'Edytuj wynik';

        $edit = BoardGamesPlays::find($id);
        $e = explode(' ', $edit['datetime']);
        $edit['date'] = $e[0];

        $score = BoardGamesPlaysScores::where('board_games_plays_id', '=', $id)->get();
        $board_games = BoardGames::where('extension','=','0')->orderBy('name')->get()->pluck('name', 'id')->all();
        $players = BoardGamesPlayers::orderBy('name')->get()->pluck('name', 'id')->all();

        return view('admin.board_games_plays.edit', compact('app', 'edit', 'score', 'board_games', 'players'));
    }

    public function update($id, Request $request)
    {
        $this->init();

        $messages = $this->validatorMessages(
            [
                'datetime.required',
            ]);

        $validator = Validator::make($request->all(),
            [
                'datetime' => 'required',
            ], $messages);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->all();
        $input['win'] = !empty($input['win']) ? 1 : 0;

        $update = BoardGamesPlays::find($id);
        $update->update($input);

        $this->saveScores($update->id, $input);

        return redirect()->route('admin.board_games_plays.edit', ['id' => $update->id])->with('success','Wpis zaktualizowany pomyślnie');
    }

    public function destroy($id)
    {
        $delete = BoardGamesPlays::find($id);
        $delete->delete();

        $delete = BoardGamesPlaysScores::where('board_games_plays_id', '=', $id);
        $delete->delete();

        return redirect()->route('admin.board_games_plays.index')->with('success', 'Wpis został usunięty');
    }

    private function saveScores($id, $request = [])
    {
        if(!empty($request))
        {
            BoardGamesPlaysScores::where('board_games_plays_id', '=', $id)->delete();

            for($i = 1; $i <= 6; $i++)
            {
                if (!empty($request['board_games_players_id_' . $i]))
                {
                    BoardGamesPlaysScores::create(
                        [
                            'board_games_plays_id' => $id,
                            'board_games_players_id' => $request['board_games_players_id_' . $i],
                            'score' => $request['score_' . $i],
                            'win' => empty($request['win_'. $i]) ? 0 : 1
                        ]
                    );
                }
            }
        }
    }

    public function filtersGame($id)
    {
        $this->init();
        $this->initController();

        $filters = session('filters');
        $filters[$this->prefix.'_games'] = $id;
        session(['filters' => $filters]);

        return redirect($this->app['buttons']['index']);
    }

    private function stats()
    {
        $stats = [];

        if(!empty($this->getFilter('games')))
        {
            $stats['players'] = BoardGamesPlaysScores::select('board_games_plays_scores.board_games_players_id', 'board_games_players.name')
                ->join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                ->join('board_games_players', 'board_games_players.id', '=', 'board_games_plays_scores.board_games_players_id')
                ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                ->groupBy('board_games_plays_scores.board_games_players_id', 'board_games_players.name')
                ->get()
                ->pluck('name', 'board_games_players_id');

            $stats['js']['players'] = BoardGamesPlaysScores::select('board_games_plays_scores.board_games_players_id', 'board_games_players.name')
                ->join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                ->join('board_games_players', 'board_games_players.id', '=', 'board_games_plays_scores.board_games_players_id')
                ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                ->groupBy('board_games_plays_scores.board_games_players_id', 'board_games_players.name')
                ->get()
                ->pluck('name', 'board_games_players_id')
                ->toArray();

            if(!empty($stats['players']))
            {
                $stats['comp']['all'] = BoardGamesPlays::join('board_games', 'board_games.id', '=', 'board_games_plays.board_game_id')
                    ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                    ->where('board_games_plays.type', '=', 0)
                    ->count();

                foreach($stats['players'] as $id => $name)
                {
                    $stats['comp']['wins'][$id] = BoardGamesPlaysScores::select('*')
                        ->join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                        ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                        ->where('board_games_plays_scores.board_games_players_id', '=', $id)
                        ->where('board_games_plays_scores.win', '=', 1)
                        ->where('board_games_plays.type', '=', 0)
                        ->count();

                    $stats['comp']['players'][$id] = BoardGamesPlaysScores::select('*')
                        ->join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                        ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                        ->where('board_games_plays_scores.board_games_players_id', '=', $id)
                        ->where('board_games_plays.type', '=', 0)
                        ->count();
                }

                $stats['comp']['top_players'] = BoardGamesPlaysScores::select('board_games_players.name', 'board_games_plays_scores.score')
                    ->join('board_games_players', 'board_games_players.id', '=', 'board_games_plays_scores.board_games_players_id')
                    ->join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                    ->where('board_games_plays.type', '=', 0)
                    ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                    ->orderBy('board_games_plays_scores.score', 'desc')
                    ->orderBy('board_games_plays.id', 'desc')
                    ->groupBy('board_games_players.name', 'board_games_plays_scores.score', 'board_games_plays.id')
                    ->take(5)
                    ->get();

                $top_games = BoardGamesPlaysScores::select('board_games_players.name', 'board_games_players.id')
                    ->join('board_games_players', 'board_games_players.id', '=', 'board_games_plays_scores.board_games_players_id')
                    ->join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                    ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                    ->where('board_games_plays.type', '=', 0)
                    ->groupBy('board_games_players.name', 'board_games_players.id')
                    ->orderBy('board_games_players.name', 'asc')
                    ->get();

                if (!empty($top_games))
                {
                    $top_games = $top_games->toArray();

                    foreach ($top_games as $key => $player)
                    {
                        $top_games[$key]['count'] = BoardGamesPlaysScores::join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                            ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                            ->where('board_games_plays.type', '=', 0)
                            ->where('board_games_plays_scores.board_games_players_id', '=', $player['id'])
                            ->count();
                    }

                    $columns = array_column($top_games, 'count');
                    array_multisort($columns, SORT_DESC, $top_games);
                }

                $stats['comp']['top_games'] = $top_games;

                // - competition

                $stats['coop']['win'] = BoardGamesPlays::join('board_games', 'board_games.id', '=', 'board_games_plays.board_game_id')
                    ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                    ->where('board_games_plays.type', '=', 1)
                    ->where('board_games_plays.win', '=', 1)
                    ->count();

                $stats['coop']['lose'] = BoardGamesPlays::join('board_games', 'board_games.id', '=', 'board_games_plays.board_game_id')
                    ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                    ->where('board_games_plays.type', '=', 1)
                    ->where('board_games_plays.win', '=', 0)
                    ->count();

                foreach($stats['players'] as $id => $name)
                {
                    $stats['coop']['wins'][$id] = BoardGamesPlaysScores::select('*')
                        ->join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                        ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                        ->where('board_games_plays_scores.board_games_players_id', '=', $id)
                        ->where('board_games_plays_scores.win', '=', 1)
                        ->where('board_games_plays.type', '=', 1)
                        ->count();

                    $stats['coop']['players'][$id] = BoardGamesPlaysScores::select('*')
                        ->join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                        ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                        ->where('board_games_plays_scores.board_games_players_id', '=', $id)
                        ->where('board_games_plays.type', '=', 1)
                        ->count();
                }

                $stats['coop']['top_players'] = BoardGamesPlaysScores::select('board_games_players.name', 'board_games_plays_scores.score')
                    ->join('board_games_players', 'board_games_players.id', '=', 'board_games_plays_scores.board_games_players_id')
                    ->join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                    ->where('board_games_plays.type', '=', 1)
                    ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                    ->orderBy('board_games_plays_scores.score', 'desc')
                    ->orderBy('board_games_plays.id', 'desc')
                    ->groupBy('board_games_players.name', 'board_games_plays_scores.score', 'board_games_plays.id')
                    ->take(5)
                    ->get();

                $top_games = BoardGamesPlaysScores::select('board_games_players.name', 'board_games_players.id')
                    ->join('board_games_players', 'board_games_players.id', '=', 'board_games_plays_scores.board_games_players_id')
                    ->join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                    ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                    ->where('board_games_plays.type', '=', 1)
                    ->groupBy('board_games_players.name', 'board_games_players.id')
                    ->orderBy('board_games_players.name', 'asc')
                    ->get();

                if (!empty($top_games))
                {
                    $top_games = $top_games->toArray();

                    foreach ($top_games as $key => $player)
                    {
                        $top_games[$key]['count'] = BoardGamesPlaysScores::join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                            ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                            ->where('board_games_plays.type', '=', 1)
                            ->where('board_games_plays_scores.board_games_players_id', '=', $player['id'])
                            ->count();
                    }

                    $columns = array_column($top_games, 'count');
                    array_multisort($columns, SORT_DESC, $top_games);
                }

                $stats['coop']['top_games'] = $top_games;

                // - solo

                $stats['solo']['win'] = BoardGamesPlays::join('board_games', 'board_games.id', '=', 'board_games_plays.board_game_id')
                    ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                    ->where('board_games_plays.type', '=', 2)
                    ->where('board_games_plays.win', '=', 1)
                    ->where('board_games_plays.no_score', '=', 0)
                    ->count();

                $stats['solo']['lose'] = BoardGamesPlays::join('board_games', 'board_games.id', '=', 'board_games_plays.board_game_id')
                    ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                    ->where('board_games_plays.type', '=', 2)
                    ->where('board_games_plays.win', '=', 0)
                    ->where('board_games_plays.no_score', '=', 0)
                    ->count();

                $stats['solo']['no_score'] = BoardGamesPlays::join('board_games', 'board_games.id', '=', 'board_games_plays.board_game_id')
                    ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                    ->where('board_games_plays.type', '=', 2)
                    ->where('board_games_plays.no_score', '=', 1)
                    ->count();

                foreach($stats['players'] as $id => $name)
                {
                    $stats['solo']['wins'][$id] = BoardGamesPlaysScores::select('*')
                        ->join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                        ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                        ->where('board_games_plays_scores.board_games_players_id', '=', $id)
                        ->where('board_games_plays_scores.win', '=', 1)
                        ->where('board_games_plays.type', '=', 2)
                        ->count();

                    $stats['solo']['players'][$id] = BoardGamesPlaysScores::select('*')
                        ->join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                        ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                        ->where('board_games_plays_scores.board_games_players_id', '=', $id)
                        ->where('board_games_plays.type', '=', 2)
                        ->count();
                }

                $stats['solo']['top_players'] = BoardGamesPlaysScores::select('board_games_players.name', 'board_games_plays_scores.score')
                    ->join('board_games_players', 'board_games_players.id', '=', 'board_games_plays_scores.board_games_players_id')
                    ->join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                    ->where('board_games_plays.type', '=', 2)
                    ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                    ->orderBy('board_games_plays_scores.score', 'desc')
                    ->orderBy('board_games_plays.id', 'desc')
                    ->groupBy('board_games_players.name', 'board_games_plays_scores.score', 'board_games_plays.id')
                    ->take(5)
                    ->get();

                $top_games = BoardGamesPlaysScores::select('board_games_players.name', 'board_games_players.id')
                    ->join('board_games_players', 'board_games_players.id', '=', 'board_games_plays_scores.board_games_players_id')
                    ->join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                    ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                    ->where('board_games_plays.type', '=', 2)
                    ->groupBy('board_games_players.name', 'board_games_players.id')
                    ->orderBy('board_games_players.name', 'asc')
                    ->get();

                if (!empty($top_games))
                {
                    $top_games = $top_games->toArray();

                    foreach ($top_games as $key => $player)
                    {
                        $top_games[$key]['count'] = BoardGamesPlaysScores::join('board_games_plays', 'board_games_plays_scores.board_games_plays_id', '=', 'board_games_plays.id')
                            ->where('board_games_plays.board_game_id', '=', $this->getFilter('games'))
                            ->where('board_games_plays.type', '=', 2)
                            ->where('board_games_plays_scores.board_games_players_id', '=', $player['id'])
                            ->count();
                    }

                    $columns = array_column($top_games, 'count');
                    array_multisort($columns, SORT_DESC, $top_games);
                }

                $stats['solo']['top_games'] = $top_games;
            }
        }

        return $stats;
    }
}
