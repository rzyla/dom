<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
//use App\Helpers\AdminHelpers;

class AdminLoginController extends AdminController
{
    public function __construct()
    {
        $this->middleware('guest:admin')->except(['logout']);
    }

    public function login(Request $request)
    {
        $this->validate($request, 
        [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
            return redirect()->intended(route('admin.dashboard'));
        }

        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function loginForm(Request $request)
    {
        return view('admin.auth.login');
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        
        return redirect('/');
    }
}
