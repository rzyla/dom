<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\FrontController;
use Illuminate\Http\Request;

class DashboardController extends FrontController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->init();
        $app = $this->app;

        return view('front.dashboard.index', compact('app'));
    }
}
