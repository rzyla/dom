<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\FrontController;
use App\ShoppingLists;
use App\ShoppingListsShops;
use Illuminate\Http\Request;

class ShoppingListsController extends FrontController
{
    public function __construct()
    {
        parent::__construct('shopping_lists');
    }

    public function index()
    {
        $this->init();
        $app = $this->app;

        $query = ShoppingLists::select('shopping_lists.*');

        //$query->join('shopping_lists_shops_lists', 'shopping_lists_shops_lists.shopping_lists_id', '=', 'shopping_lists.id')
        //    ->where('shopping_lists_shops_lists.shopping_lists_shops_id', '=', $this->getFilter('shops'));

        $query->leftJoin('shopping_lists_positions', function ($join) {

            $shop_id = !empty($this->app['filters']['shops']['selected']) ? $this->app['filters']['shops']['selected'] : 0;

            $join->on('shopping_lists_positions.shopping_lists_id', '=', 'shopping_lists.id')
                ->where('shopping_lists_positions.shopping_lists_shops_id', '=', $shop_id);
        });

        $query->orderBy('shopping_lists_positions.position', 'asc');

        $grid = $query->paginate(10000);

        foreach($grid as $key => $val)
        {
            $shops = ShoppingListsShops::select('*')
                ->join('shopping_lists_shops_lists', 'shopping_lists_shops_lists.shopping_lists_shops_id', '=', 'shopping_lists_shops.id')
                ->where('shopping_lists_shops_lists.shopping_lists_id', '=', $val['id'])
                ->get()
                ->pluck('name');

            $grid[$key]['shops'] = !empty($shops) ? implode(', ', $shops->toArray()) : '';
        }

        return view('front.shopping_lists.index', compact('app', 'grid'));
    }
}
