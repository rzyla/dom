<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pages extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';

    protected $fillable = 
    [
        'title', 'lead', 'description', 'active'
    ];
}
