<?php

namespace App\Helpers;

use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ImageHelper
{
    static function Thumb($file, $width, $height)
    {
        $path = public_path($file);

        if(File::exists($path))
        {
            $thumbInfo = pathinfo($path);
            $thumbUrl = '/thumbs/'.str_replace($thumbInfo['basename'], $width.'-'.$height.'-'.$thumbInfo['basename'], $file);
            $thumbFile = public_path('thumbs/'.str_replace($thumbInfo['basename'], $width.'-'.$height.'-'.$thumbInfo['basename'], $file));

            if(!File::exists($thumbFile))
            {
                $thumbInfo = pathinfo($thumbFile);

                if(!File::exists($thumbInfo['dirname']))
                {
                    File::makeDirectory($thumbInfo['dirname'], 0755, true);
                }

                $thumb = Image::make($path);
                $org_width = Image::make($path)->width();
                $org_height = Image::make($path)->height();

                if($org_width > $org_height)
                {
                    $procent = $height / $org_height;

                    if(($org_width * $procent) < $width)
                    {
                        $thumb->widen($width);
                    }
                    else
                    {
                        $thumb->heighten($height);
                    }
                }
                else
                {
                    $procent = $width / $org_width;

                    if(($org_height * $procent) < $height)
                    {
                        $thumb->heighten($height);
                    }
                    else
                    {
                        $thumb->widen($width);
                    }
                }

                $thumb->crop($width, $height);
                $thumb->save($thumbFile);
            }

            echo $thumbUrl;
        }
    }
}
?>
