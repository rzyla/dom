<?php

namespace App\Helpers;

class AdminHtmlHelper
{
    static function DisplayErrorsDiv($errors, $field, $message = "", $class = "", $style = "")
    {
        if(!empty($errors->first($field)))
        {
            $class = !empty($class) ? ' class="'.$class.'"' : '';
            $style = !empty($style) ? ' class="'.$style.'"' : '';

            echo '<div'.$class.$style.'>'.$message.'</div>';
        }
    }

    static function DisplayErrorsClass($errors, $field, $class = "")
    {
        if(!empty($errors->first($field)))
        {
            echo $class;
        }
    }
}
?>