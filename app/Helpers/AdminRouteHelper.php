<?php

namespace App\Helpers;

class AdminRouteHelper
{
    static function RouteResource($name)
    {
        return 
        [
            'names' => [
                'index' => 'admin.'.$name.'.index',
                'create' => 'admin.'.$name.'.create',
                'store' => 'admin.'.$name.'.create.submit',
                'edit' => 'admin.'.$name.'.edit',
                'update' => 'admin.'.$name.'.edit.submit',
                'destroy' => 'admin.'.$name.'.destroy',
            ],
            'parameters' => 
            [
                'edit' => 'id', 
                'update' => 'id', 
                'destroy' => 'id'
            ]
        ];
    }
}
?>