<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\NotesGroups;

class Notes extends Model
{
    use Notifiable;

    protected $guard = 'admin';
    protected $table = 'notes';

    protected $fillable = 
    [
        'notes_groups_id', 'title', 'description', 'dashboard'
    ];

    public static function getDashboardNotes()
    {
        $ids = Notes::select('notes.notes_groups_id')->where('dashboard', '=', 1)->distinct('notes_groups_id')->get()->pluck('notes_groups_id')->all();
        $notes = [];
        
        foreach($ids as $id)
        {
            $group = NotesGroups::find($id);

            $notes[] = 
            [
                'category' => $group['name'],
                'items' => Notes::where('dashboard', '=', 1)->where('notes_groups_id', '=', $id)->get()
            ];
        }

        return $notes;
    }
}
