@extends('front.layout')

@section('content')

    <table class="table table-grid radius-border">
        <thead>
        <tr>
            <th></th>
            <th>Nazwa</th>
            <th>Sklepy</th>
            <th>Ilość</th>
            <th>Ważne</th>
        </tr>
        </thead>
        <tbody class="sortable">
        @foreach ($grid as $key => $item)
            <tr>
                <td class="width-50">
                    @if(!empty($item->image))
                        <img src="{{ ImageHelper::Thumb($app['current']['uploadSourceDir'].$item->image, 50, 50) }}" alt="" />
                    @endif
                </td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->shops }}</td>
                <td class="product-update width-250">
                    {{ $item->quantity }}
                </td>
                <td class="width-50">
                    <form method="POST" action="{{ route('admin.shopping_lists.ajax_quantity_priority.submit') }}">
                        <div class="custom-control custom-switch">
                            @if($item->prio == 1)
                                <input type="checkbox" class="custom-control-input jq-ajax-query-priority" id="customSwitch-{{$item->id}}" data-id="{{$item->id}}" data-href="{{ route('admin.shopping_lists.ajax_quantity_priority.submit') }}" checked>
                            @else
                                <input type="checkbox" class="custom-control-input jq-ajax-query-priority" id="customSwitch-{{$item->id}}" data-id="{{$item->id}}" data-href="{{ route('admin.shopping_lists.ajax_quantity_priority.submit') }}">
                            @endif
                            <label class="custom-control-label" for="customSwitch-{{$item->id}}">&nbsp;</label>
                        </div>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
