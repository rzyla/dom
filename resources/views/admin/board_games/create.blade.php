@extends('admin.layout')

@section('content')

    <form method="POST" action="{{ route('admin.board_games.create.submit') }}" enctype="multipart/form-data">
        @csrf
        <div class="card-body white-background silver-border radius-border-top">

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'name', 'input-error jq-input-error') }}">
                <label for="name">Nazwa</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ old('name') }}">
            </div>

            <div class="form-group">
                <label for="board_game_id">Połączona gra</label>
                <select class="form-control" name="board_game_id">
                    <option value="0"></option>
                    @foreach($board_games as $key => $val)
                        <option value="{{ $key }}">{{ $val }}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="form-group">
                <label for="board_game_id">Typ gry</label>
                <select class="form-control" name="type">
                    <option value="1">Rywalizacyjna</option>
                    <option value="2">Kooperacyjna</option>
                </select>
            </div>

            <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" name="wishlist" id="wishlist" value="1">
                        <label for="wishlist" class="custom-control-label">Lista życzeń</label>
                    </div>
            </div>

            <div class="form-group">
                <label for="exampleInputFile">Zdjęcie</label>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="image" name="image" accept="image/*">
                        <label class="custom-file-label" for="image"></label>
                    </div>
                </div>
            </div>

        </div>
        <div class="card-footer silver-border radius-border-bottom">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </form>

@endsection
