@extends('admin.layout')

@section('content')

    <form method="POST" action="{{ route('admin.board_games.edit.submit', $edit->id) }}" enctype="multipart/form-data">
        @csrf
        <input name="_method" type="hidden" value="PUT">
        <div class="card-body white-background silver-border radius-border-top">

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'name', 'input-error jq-input-error') }}">
                <label for="name">Nazwa</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ !empty(old('name')) ? old('name') : $edit->name }}">
            </div>

            <div class="form-group">
                <label for="board_game_id">Połączona gra</label>
                <select class="form-control" name="board_game_id">
                    <option value="0"></option>
                    @foreach($board_games as $key => $val)
                        <option value="{{ $key }}" @if($key == $edit->board_game_id) selected @endif>{{ $val }}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="form-group">
                <label for="board_game_id">Typ gry</label>
                <select class="form-control" name="type">
                    <option value="1" @if($edit->type == 1) selected @endif>Rywalizacyjna</option>
                    <option value="2" @if($edit->type == 2) selected @endif>Kooperacyjna</option>
                </select>
            </div>

            <div class="form-group">
                <div class="custom-control custom-checkbox">

                    @if(!empty(old('prio')))
                        <input class="custom-control-input" type="checkbox" name="wishlist" id="wishlist" value="1" @if(!empty(old('wishlist'))) checked @endif>
                    @else
                        <input class="custom-control-input" type="checkbox" name="wishlist" id="wishlist" value="1" @if(!empty($edit->wishlist)) checked @endif>
                    @endif

                    <label for="wishlist" class="custom-control-label">Lista życzeń</label>
                </div>
            </div>

            <div class="form-group">
                <label for="exampleInputFile">Zdjęcie</label>
                @if(empty($edit->image))
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="image" name="image" accept="image/*">
                        <label class="custom-file-label" for="image"></label>
                    </div>
                </div>
                @else
                <div>
                     <img src="{{ ImageHelper::Thumb($app['current']['uploadSourceDir'].$edit->image, 250, 250) }}" alt="" />
                </div>
                <input type="checkbox" value="delete" name="deleteImage" /> Usuń
                @endif
            </div>

        </div>
        <div class="card-footer silver-border radius-border-bottom">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </form>

@endsection
