@extends('admin.layout')

@section('scripts')

    <script type="text/javascript">
        $( document ).ready(function()
        {
            $(".jq-on-change-submit#show option:first-child").remove();
            $(".jq-on-change-submit#addons option:first-child").remove();
        });
    </script>

@endsection

@section('content')

    <div class="row">
    @foreach($board_games as $key => $item)
        <div class="col-6 col-sm-4 col-md-2 d-flex align-items-stretch board-games">
            <div class="card">
                <div class="card-body pt-0">
                    <div class="name d-flex">
                        <div class="align-self-center">{{ $item->name }}</div>
                    </div>
                    <div class="image">
                        @if(!empty($item->image))
                            <img src="{{ ImageHelper::Thumb($app['current']['uploadSourceDir'].$item->image, 250, 250) }}" alt="" class="img-fluid" />
                        @endif
                    </div>
                </div>
                <div class="card-footer clearfix">
                    <div class="float-left">
                        <a class="btn btn-sm btn-primary" href="{{ route('admin.board_games.edit', $item->id) }}" title="Edytuj"><i class="fas fa-pencil-alt"></i></a>
                        <a class="btn btn-sm btn-danger jq-popup-congrim" data-submit-class="jq-delete-items-form-{{ $item->id }}" href="#" title="Usuń"><i class="fas fa-trash-alt"></i></a>
                        <form method="POST" action="{{ route('admin.board_games.destroy', $item->id) }}" class="jq-delete-items-form-{{ $item->id }}">
                            <input name="_method" type="hidden" value="DELETE">
                            @csrf
                        </form>
                    </div>
                    <div class="float-right">
                    	<!--<span class="btn btn-sm btn-dark"><i class="fas @if($item->type == 1) fa-rocket @else fa-paw @endif"></i></span>-->
                        @if(empty($item->board_game_id))
                            <a class="btn btn-sm btn-success" href="{{ route('admin.board_games_plays.filters.game', $item->id) }}" title="Wyniki"><i class="fas fa-trophy"></i> &nbsp; {{$item->plays}}</a>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    @endforeach

    </div>

    @if(!empty($wish_list))

        @if(!empty($wish_list) && !empty($board_games))
            <div class="margin-top-20 margin-bottom-20">Lisa życzeń</div>
        @endif

        <div class="row">
            @foreach($wish_list as $key => $item)
                <div class="col-6 col-sm-4 col-md-2 d-flex align-items-stretch board-games">
                    <div class="card">
                        <div class="card-body pt-0">
                            <div class="name d-flex">
                                <div class="align-self-center">{{ $item->name }}</div>
                            </div>
                            <div class="image">
                                @if(!empty($item->image))
                                    <img src="{{ ImageHelper::Thumb($app['current']['uploadSourceDir'].$item->image, 250, 250) }}" alt="" class="img-fluid" />
                                @endif
                            </div>
                        </div>
                        <div class="card-footer clearfix">
                            <div class="float-left">
                                <a class="btn btn-sm btn-primary" href="{{ route('admin.board_games.edit', $item->id) }}" title="Edytuj"><i class="fas fa-pencil-alt"></i></a>
                                <a class="btn btn-sm btn-danger jq-popup-congrim" data-submit-class="jq-delete-items-form-{{ $item->id }}" href="#" title="Usuń"><i class="fas fa-trash-alt"></i></a>
                                <form method="POST" action="{{ route('admin.board_games.destroy', $item->id) }}" class="jq-delete-items-form-{{ $item->id }}">
                                    <input name="_method" type="hidden" value="DELETE">
                                    @csrf
                                </form>
                            </div>
                            <div class="float-right">
                                @if(empty($item->board_game_id))
                                    <a class="btn btn-sm btn-success" href="{{ route('admin.board_games_plays.filters.game', $item->id) }}" title="Wyniki"><i class="fas fa-trophy"></i></a>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            @endforeach
        </div>

    @endif

    <div class="table-grid margin-bottom-20">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    Wszystkich posiadanych gier: <strong>{{ $stats['board_games']['all'] }}</strong><br/>
                     - gier podstawowych: <strong>{{ $stats['board_games']['games'] }}</strong><br/>
                     - dodatków: <strong>{{ $stats['board_games']['addons'] }}</strong>
                </div>
                <div class="col-sm-6">
                    Lista życzeń: <strong>{{ $stats['wish_list']['all'] }}</strong><br/>
                    - gier podstawowych: <strong>{{ $stats['wish_list']['games'] }}</strong><br/>
                    - dodatków: <strong>{{ $stats['wish_list']['addons'] }}</strong>
                </div>
            </div>
        </div>
    </div>

@endsection
