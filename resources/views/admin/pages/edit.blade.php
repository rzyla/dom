@extends('admin.layout')

@section('content')

    <form method="POST" action="{{ route('admin.pages.edit.submit', $edit->id) }}">
        @csrf
        <input name="_method" type="hidden" value="PUT">

        <div class="card-body white-background silver-border radius-border-top">

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'title', 'input-error jq-input-error') }}">
                <label for="title">Tytuł</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="" value="{{ !empty(old('title')) ? old('title') : $edit->title }}">
            </div>

            <div class="form-group">
                <label for="title">Krótki opis</label>
                <textarea class="summernote form-control" name="lead" id="lead" placeholder="">{{ !empty(old('lead')) ? old('lead') : $edit->lead }}</textarea>
            </div>

            <div class="form-group">
                <label for="title">Długi opis</label>
                <textarea class="summernote800 form-control" name="description" id="description" placeholder="">{{ !empty(old('description')) ? old('description') : $edit->description }}</textarea>
            </div>

            <div class="form-group">
                <label for="active">Aktywny</label>
                <select class="form-control" name="active">
                    <option value="0" @if(0 == $edit->active) selected @endif>Nie</option>
                    <option value="1" @if(1 == $edit->active) selected @endif>Tak</option>
                </select>
            </div>

        </div>
        <div class="card-header silver-border">
            <h3 class="card-title ">SEO</h3>
        </div>
        <div class="card-body white-background silver-border radius-border-top">

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'url', 'input-error jq-input-error') }}">
                <label for="url">Url</label>
                <input type="text" class="form-control" id="url" name="url" placeholder="" value="{{ !empty(old('url')) ? old('url') : $edit->url }}">
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'meta_title', 'input-error jq-input-error') }}">
                <label for="meta_title">Tytuł</label>
                <input type="text" class="form-control" id="meta_title" name="meta_title" placeholder="" value="{{ !empty(old('meta_title')) ? old('meta_title') : $edit->meta_title }}">
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'meta_description', 'input-error jq-input-error') }}">
                <label for="meta_description">Opis</label>
                <input type="text" class="form-control" id="meta_description" name="meta_description" placeholder="" value="{{ !empty(old('meta_description')) ? old('meta_description') : $edit->meta_description }}">
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'meta_key_words', 'input-error jq-input-error') }}">
                <label for="meta_key_words">Słowa kluczowe</label>
                <input type="text" class="form-control" id="meta_key_words" name="meta_key_words" placeholder="" value="{{ !empty(old('meta_key_words')) ? old('meta_key_words') : $edit->meta_key_words }}">
            </div>

        </div>
  
        <div class="card-footer silver-border radius-border-bottom">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>

    </form>

@endsection

@section('styles')
    <link href="{{ asset('assets/admin/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet">
@endsection

@section('scripts')
    <script src="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(function () 
        {
            $('.summernote').summernote(
            {
                height: 150
            });

            $('.summernote800').summernote(
            {
                height: 800
            });
        })
    </script>
@endsection