@extends('admin.layout')

@section('content')

    <table class="table table-grid radius-border">
        <tbody>
            <tr>
                <th>#</th>
                <th>Tytuł</th>
                <th>Aktywny</th>
                <th></th>
            </tr>
            @foreach ($grid as $key => $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->title }}</td>
                <td>
                    @if($item->active == 1)
                        Aktywny
                    @else
                        Nieaktywny
                    @endif
                </td>
                <td class="text-right">
                    <a class="btn btn-sm btn-primary" href="{{ route('admin.pages.edit', $item->id) }}">Edytuj</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {{ $grid->render() }}

@endsection