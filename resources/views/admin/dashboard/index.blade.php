@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-lg-3 col-6"></div>
    <div class="col-lg-3 col-6"></div>
    <div class="col-lg-3 col-6"></div>

    <div class="col-lg-3 col-6">

        <!--<div class="card">
            <div class="card-header border-transparent">
                <h3 class="card-title">Notatki</h3>
                <div class="card-tools">
                    <a class="btn btn-tool" href="{{ route('admin.notes.create') }}" data-toggle="tab"><i class="nav-icon fas fa-plus"></i></a>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    @if(!empty($notes))
                        <table class="table m-0">
                            @foreach($notes as $note)
                                <thead>
                                    <tr>
                                        <th>{{ $note['category'] }}</th>
                                    </tr>
                                </thead>
                                @if(!empty($note['items']))
                                    <tbody>
                                        @foreach($note['items'] as $item)
                                            <tr>
                                                <td><a href="{{ route('admin.notes.edit', $item->id) }}">{{ $item->title }}</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                @endif
                            @endforeach
                        </table>
                    @endif
                </div>
            </div>
        </div>-->

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Notatki</h3>
                <div class="card-tools">
                    <ul class="nav nav-pills ml-auto">
                        <li class="nav-item">
                            <a class="btn btn-block btn-primary btn-sm" href="{{ route('admin.notes.create') }}"><i class="nav-icon fas fa-plus"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content p-0">
                    @if(!empty($notes))
                            @foreach($notes as $note)
                                <div><strong>{{ $note['category'] }}</strong></div>
                                @if(!empty($note['items']))
                                        @foreach($note['items'] as $item)
                                            <div><a href="{{ route('admin.notes.edit', $item->id) }}">{{ $item->title }}</a></div>
                                        @endforeach
                                @endif
                            @endforeach
                    @endif
                </div>
            </div>
        </div>

    </div>

</div>
@endsection
