@extends('admin.layout')

@section('content')

    <form method="POST" action="{{ route('admin.account.edit.submit', $edit->id) }}" enctype="multipart/form-data">
        @csrf
        <input name="_method" type="hidden" value="PUT">
        <div class="card-body white-background silver-border radius-border-top">

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'name', 'input-error jq-input-error') }}">
                <label for="name">Imię i nazwisko</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ !empty(old('name')) ? old('name') : $edit->name }}">
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'email', 'input-error jq-input-error') }}">
                <label for="email">Adres e-mail</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="" value="{{ !empty(old('email')) ? old('email') : $edit->email }}">
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'password', 'input-error jq-input-error') }}">
                <label for="password">Hasło <small class="text-danger">(Pozostaw puste jeśli nie chcesz modyfikować)</small></label>
                <input type="password" class="form-control" id="password" name="password" placeholder="">
                <div><small>Minimalna ilość znaków: 6</small></div>
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'email', 'input-error jq-input-error') }}">
                <label for="email">Zdjęcie</label>
                @if(empty($edit->avatar))
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="avatar" name="avatar" />
                    <label class="custom-file-label" for="avatar"></label>
                </div>
                @else
                <div>
                    <img src="{{ ImageHelper::Thumb($app['user']['avatar'], 200, 200) }}" alt="" />
                </div>
                <input type="checkbox" value="delete" name="deleteAvatar" /> Usuń
                @endif
            </div>
        </div>
        <div class="card-footer silver-border radius-border-bottom">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </form>

@endsection
