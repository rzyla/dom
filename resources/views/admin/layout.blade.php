<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
        <link href="{{ asset('assets/admin/css/admin.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/admin/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/admin/images/favico.ico') }}"  rel="shortcut icon">

        @yield('styles')

        <title>{{ $app['meta']['title'] }}</title>
    </head>
    <body class="hold-transition sidebar-mini layout-fixed">

        <div class="wrapper">

          @include('admin._partials.navbar')

          <div class="content-wrapper">
            @include('admin._partials.sidebar')
            @include('admin._partials.breadcrumb')

            <section class="content">
              <div class="container-fluid">
                @include('admin._partials.alerts')
                @yield('content')
              </div>
            </section>
          </div>
        </div>

        <footer class="main-footer">
          2008 - {{ date("Y") }} &copy; ARTEH Agencja Interaktywna
        </footer>

        <aside class="control-sidebar control-sidebar-dark">
        </aside>

        @include('admin._partials.modals.delete')
        @include('admin._partials._scripts')

        @yield('scripts')

        <script src="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
        <script type="text/javascript">
        $(function ()
        {
          $('.summernote').summernote({ height: 150 });
          $('.summernote400').summernote({ height: 400 });
          $('.summernote800').summernote({ height: 800 });
        })
    </script>

    </body>
</html>
