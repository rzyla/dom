@extends('admin.layout')

@section('scripts')
    <script type="text/javascript">

        $(document).ready(function()
        {
            showWinFieldCheckbox();

            $('.jq-coop-solo-win-type').change(function()
            {
                showWinFieldCheckbox();
            });
        });

        function showWinFieldCheckbox()
        {
            if($('.jq-coop-solo-win-type option:selected').val() != "0")
            {
                $(".jq-coop-solo-win").show();
                $('.jq-coop-solo-no-score').show();
            }
            else
            {
                $(".jq-coop-solo-win").hide();
                $('.jq-coop-solo-no-score').hide();
            }
        }

    </script>
@endsection

@section('content')

    <form method="POST" action="{{ route('admin.board_games_plays.edit.submit', $edit->id) }}" enctype="multipart/form-data">
        @csrf
        <input name="_method" type="hidden" value="PUT">
        <div class="card-body white-background silver-border radius-border-top">

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'datetime', 'input-error jq-input-error') }}">
                <label for="datetime">Data</label>
                <input type="text" class="form-control datepicker" id="datetime" name="datetime" placeholder="" value="{{ !empty(old('datetime')) ? old('datetime') : $edit->date }}">
            </div>

            <div class="form-group">
                <label for="board_game_id">Połączona gra</label>
                <select class="form-control" name="board_game_id">
                    <option value="0"></option>
                    @foreach($board_games as $key => $val)
                        <option value="{{ $key }}" @if($key == $edit->board_game_id) selected @endif>{{ $val }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="board_game_id">Typ rozgrywki</label>
                <select class="form-control jq-coop-solo-win-type" name="type" >
                    <option value="0" @if($edit->type == 0) selected @endif>Rywalizacja</option>
                    <option value="1" @if($edit->type == 1) selected @endif>Kooperacja</option>
                    <option value="2" @if($edit->type == 2) selected @endif>Solo</option>
                </select>
            </div>

            <div class="custom-control custom-checkbox margin-top-10 jq-coop-solo-win" style="display: none;">
                <input class="custom-control-input" type="checkbox" name="win" id="win" value="1"  @if($edit->win == 1) checked @endif>
                <label for="win" class="custom-control-label">Wygrana</label>
            </div>

            <div class="custom-control custom-checkbox margin-top-10 jq-coop-solo-no-score" style="display: none;">
                <input class="custom-control-input" type="checkbox" name="no_score" id="no_score" value="1"  @if($edit->no_score == 1) checked @endif>
                <label for="no_score" class="custom-control-label">Brak wyniku</label>
            </div>

            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 margin-top-20">
                    <!-- 1 -->
                    <div class="form-group margin-bottom-0">
                        <label for="board_games_players_id_1">Gracz 1</label>
                        <select class="form-control" name="board_games_players_id_1">
                            <option value="0"></option>
                            @foreach($players as $key => $val)
                                <option value="{{ $key }}" @if(!empty($score[0]) && $key == $score[0]->board_games_players_id) selected @endif>{{ $val }}</option>
                            @endforeach
                        </select>
                        <div class="margin-top-10">
                            <input type="text" class="form-control" id="score_1" name="score_1" placeholder="Wynik" @if(!empty($score[0])) value="{{$score[0]->score}}" @else value="" @endif>
                        </div>

                        <div class="custom-control custom-checkbox margin-top-10">
                            <input class="custom-control-input" type="checkbox" name="win_1" id="win_1" value="1" @if(!empty($score[0]) && 1 == $score[0]->win) checked @endif>
                            <label for="win_1" class="custom-control-label">Wygrana</label>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 margin-top-20">
                    <!-- 2 -->
                    <div class="form-group margin-bottom-0">
                        <label for="board_games_players_id_2">Gracz 2</label>
                        <select class="form-control" name="board_games_players_id_2">
                            <option value="0"></option>
                            @foreach($players as $key => $val)
                                <option value="{{ $key }}" @if(!empty($score[1]) && $key == $score[1]->board_games_players_id) selected @endif>{{ $val }}</option>
                            @endforeach
                        </select>
                        <div class="margin-top-10">
                            <input type="text" class="form-control" id="score_2" name="score_2" placeholder="Wynik" @if(!empty($score[1])) value="{{$score[1]->score}}" @else value="" @endif>
                        </div>

                        <div class="custom-control custom-checkbox margin-top-10">
                            <input class="custom-control-input" type="checkbox" name="win_2" id="win_2" value="1" @if(!empty($score[1]) && 1 == $score[1]->win) checked @endif>
                            <label for="win_2" class="custom-control-label">Wygrana</label>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 margin-top-20">
                    <!-- 3 -->
                    <div class="form-group margin-bottom-0">
                        <label for="board_games_players_id_3">Gracz 3</label>
                        <select class="form-control" name="board_games_players_id_3">
                            <option value="0"></option>
                            @foreach($players as $key => $val)
                                <option value="{{ $key }}" @if(!empty($score[2]) && $key == $score[2]->board_games_players_id) selected @endif>{{ $val }}</option>
                            @endforeach
                        </select>
                        <div class="margin-top-10">
                            <input type="text" class="form-control" id="score_3" name="score_3" placeholder="Wynik" @if(!empty($score[2])) value="{{$score[2]->score}}" @else value="" @endif>
                        </div>

                        <div class="custom-control custom-checkbox margin-top-10">
                            <input class="custom-control-input" type="checkbox" name="win_3" id="win_3" value="1" @if(!empty($score[2]) && 1 == $score[2]->win) checked @endif>
                            <label for="win_3" class="custom-control-label">Wygrana</label>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 margin-top-20">
                    <!-- 4 -->
                    <div class="form-group margin-bottom-0">
                        <label for="board_games_players_id_4">Gracz 4</label>
                        <select class="form-control" name="board_games_players_id_4">
                            <option value="0"></option>
                            @foreach($players as $key => $val)
                                <option value="{{ $key }}" @if(!empty($score[3]) && $key == $score[3]->board_games_players_id) selected @endif>{{ $val }}</option>
                            @endforeach
                        </select>
                        <div class="margin-top-10">
                            <input type="text" class="form-control" id="score_4" name="score_4" placeholder="Wynik" @if(!empty($score[3])) value="{{$score[3]->score}}" @else value="" @endif>
                        </div>

                        <div class="custom-control custom-checkbox margin-top-10">
                            <input class="custom-control-input" type="checkbox" name="win_4" id="win_4" value="1" @if(!empty($score[3]) && 1 == $score[3]->win) checked @endif>
                            <label for="win_4" class="custom-control-label">Wygrana</label>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 margin-top-20">
                    <!-- 5 -->
                    <div class="form-group margin-bottom-0">
                        <label for="board_games_players_id_5">Gracz 5</label>
                        <select class="form-control" name="board_games_players_id_5">
                            <option value="0"></option>
                            @foreach($players as $key => $val)
                                <option value="{{ $key }}" @if(!empty($score[4]) && $key == $score[4]->board_games_players_id) selected @endif>{{ $val }}</option>
                            @endforeach
                        </select>
                        <div class="margin-top-10">
                            <input type="text" class="form-control" id="score_5" name="score_5" placeholder="Wynik" @if(!empty($score[4])) value="{{$score[4]->score}}" @else value="" @endif>
                        </div>

                        <div class="custom-control custom-checkbox margin-top-10">
                            <input class="custom-control-input" type="checkbox" name="win_5" id="win_5" value="1" @if(!empty($score[4]) && 1 == $score[4]->win) checked @endif>
                            <label for="win_5" class="custom-control-label">Wygrana</label>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 margin-top-20">
                    <!-- 6 -->
                    <div class="form-group margin-bottom-0">
                        <label for="board_games_players_id_6">Gracz 6</label>
                        <select class="form-control" name="board_games_players_id_6">
                            <option value="0"></option>
                            @foreach($players as $key => $val)
                                <option value="{{ $key }}" @if(!empty($score[5]) && $key == $score[5]->board_games_players_id) selected @endif>{{ $val }}</option>
                            @endforeach
                        </select>
                        <div class="margin-top-10">
                            <input type="text" class="form-control" id="score_6" name="score_6" placeholder="Wynik" @if(!empty($score[5])) value="{{$score[5]->score}}" @else value="" @endif>
                        </div>

                        <div class="custom-control custom-checkbox margin-top-10">
                            <input class="custom-control-input" type="checkbox" name="win_6" id="win_6" value="1" @if(!empty($score[5]) && 1 == $score[5]->win) checked @endif>
                            <label for="win_6" class="custom-control-label">Wygrana</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="card-footer silver-border radius-border-bottom">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </form>

@endsection
