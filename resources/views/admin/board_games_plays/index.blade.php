@extends('admin.layout')

@section('scripts')

    @if(!empty($stats))
        <script src="/assets/admin/chart.js/Chart.min.js"></script>
        <script type="text/javascript">

            var colors = ['#00c0ef', '#3c8dbc', '#f39c12', '#d2d6de', '#00a65a', '#f56954'];

            // -- comp

            var pieChartCanvas = $('#pieChartComp1').get(0).getContext('2d')
            var pieData = {
                labels: [ @if(!empty($stats['js']['players'])) '{!! implode("','", $stats['js']['players']) !!}' @endif ],
                datasets: [
                    {
                        data: [@if(!empty($stats['comp']['wins'])) '{!! implode("','", $stats['comp']['wins']) !!}' @endif],
                        backgroundColor: colors
                    }]}
            var pieOptions = {legend: {display: true}}
            var pieChart = new Chart(pieChartCanvas, {type: 'doughnut', data: pieData, options: pieOptions})

            var pieChartCanvas = $('#pieChartComp2').get(0).getContext('2d')
            var pieData = {
                labels: [ @if(!empty($stats['js']['players'])) '{!! implode("','", $stats['js']['players']) !!}' @endif ],
                datasets: [
                    {
                        data: [@if(!empty($stats['comp']['players'])) '{!! implode("','", $stats['comp']['players']) !!}' @endif],
                        backgroundColor: colors
                    }]}
            var pieOptions = {legend: {display: true}}
            var pieChart = new Chart(pieChartCanvas, {type: 'doughnut', data: pieData, options: pieOptions})

            // -- coop

            var pieChartCanvas = $('#pieChartCoop1').get(0).getContext('2d')
            var pieData = {
                labels: [ @if(!empty($stats['js']['players'])) '{!! implode("','", $stats['js']['players']) !!}' @endif ],
                datasets: [
                    {
                        data: [@if(!empty($stats['coop']['wins'])) '{!! implode("','", $stats['coop']['wins']) !!}' @endif],
                        backgroundColor: colors
                    }]}
            var pieOptions = {legend: {display: true}}
            var pieChart = new Chart(pieChartCanvas, {type: 'doughnut', data: pieData, options: pieOptions})

            var pieChartCanvas = $('#pieChartCoop2').get(0).getContext('2d')
            var pieData = {
                labels: [ @if(!empty($stats['js']['players'])) '{!! implode("','", $stats['js']['players']) !!}' @endif ],
                datasets: [
                    {
                        data: [@if(!empty($stats['coop']['players'])) '{!! implode("','", $stats['coop']['players']) !!}' @endif],
                        backgroundColor: colors
                    }]}
            var pieOptions = {legend: {display: true}}
            var pieChart = new Chart(pieChartCanvas, {type: 'doughnut', data: pieData, options: pieOptions})

            // -- solo

            var pieChartCanvas = $('#pieChartSolo1').get(0).getContext('2d')
            var pieData = {
                labels: [ @if(!empty($stats['js']['players'])) '{!! implode("','", $stats['js']['players']) !!}' @endif ],
                datasets: [
                    {
                        data: [@if(!empty($stats['solo']['wins'])) '{!! implode("','", $stats['solo']['wins']) !!}' @endif],
                        backgroundColor: colors
                    }]}
            var pieOptions = {legend: {display: true}}
            var pieChart = new Chart(pieChartCanvas, {type: 'doughnut', data: pieData, options: pieOptions})

            var pieChartCanvas = $('#pieChartSolo2').get(0).getContext('2d')
            var pieData = {
                labels: [ @if(!empty($stats['js']['players'])) '{!! implode("','", $stats['js']['players']) !!}' @endif ],
                datasets: [
                    {
                        data: [@if(!empty($stats['solo']['players'])) '{!! implode("','", $stats['solo']['players']) !!}' @endif],
                        backgroundColor: colors
                    }]}
            var pieOptions = {legend: {display: true}}
            var pieChart = new Chart(pieChartCanvas, {type: 'doughnut', data: pieData, options: pieOptions})

        </script>
    @endif

@endsection

@section('content')

    @if(!empty($stats))

    <div class="row">

        <div class="col-6 col-sm-4 col-md-4 d-flex align-items-stretch">
            <div class="card" style="width: 100%">
                <div class="card-header">
                    <h3 class="card-title clear-fix" style="width: 100%;">
                        <span class="float-left">Rywalizacja</span>
                        <span class="float-right"><i class="fas fa-chess-rook"></i> &nbsp; {{$stats['comp']['all']}}</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="chart-responsive">
                                <canvas id="pieChartComp1" height="250"></canvas>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="chart-responsive">
                                <canvas id="pieChartComp2" height="250"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer bg-white p-0">
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="nav nav-pills flex-column margin-top-20">
                                @if(!empty($stats['comp']['top_players'][0]))
                                <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: gold;"></i> &nbsp; {{$stats['comp']['top_players'][0]['name']}}
                                        <strong class="float-right">{{$stats['comp']['top_players'][0]['score']}}</strong>
                                    </span>
                                </li>
                                @endif
                                @if(!empty($stats['comp']['top_players'][1]))
                                <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.5"></i> &nbsp; {{$stats['comp']['top_players'][1]['name']}}
                                        <strong class="float-right">{{$stats['comp']['top_players'][1]['score']}}</strong>
                                    </span>
                                </li>
                                @endif
                                @if(!empty($stats['comp']['top_players'][2]))
                                <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.4"></i> &nbsp; {{$stats['comp']['top_players'][2]['name']}}
                                        <strong class="float-right">{{$stats['comp']['top_players'][2]['score']}}</strong>
                                    </span>
                                </li>
                                @endif
                                @if(!empty($stats['comp']['top_players'][3]))
                                <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.3"></i> &nbsp; {{$stats['comp']['top_players'][3]['name']}}
                                        <strong class="float-right">{{$stats['comp']['top_players'][3]['score']}}</strong>
                                    </span>
                                </li>
                                @endif
                                @if(!empty($stats['comp']['top_players'][4]))
                                <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.2"></i> &nbsp; {{$stats['comp']['top_players'][4]['name']}}
                                        <strong class="float-right">{{$stats['comp']['top_players'][4]['score']}}</strong>
                                    </span>
                                </li>
                                @endif
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="nav nav-pills flex-column margin-top-20">
                                @if(!empty($stats['comp']['top_games'][0]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: gold;"></i> &nbsp; {{$stats['comp']['top_games'][0]['name']}}
                                        <strong class="float-right">{{$stats['comp']['top_games'][0]['count']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['comp']['top_games'][1]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.5"></i> &nbsp; {{$stats['comp']['top_games'][1]['name']}}
                                        <strong class="float-right">{{$stats['comp']['top_games'][1]['count']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['comp']['top_games'][2]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.4"></i> &nbsp; {{$stats['comp']['top_games'][2]['name']}}
                                        <strong class="float-right">{{$stats['comp']['top_games'][2]['count']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['comp']['top_games'][3]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.3"></i> &nbsp; {{$stats['comp']['top_games'][3]['name']}}
                                        <strong class="float-right">{{$stats['comp']['top_games'][3]['count']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['comp']['top_games'][4]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.2"></i> &nbsp; {{$stats['comp']['top_games'][4]['name']}}
                                        <strong class="float-right">{{$stats['comp']['top_games'][4]['count']}}</strong>
                                    </span>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-4 d-flex align-items-stretch">
            <div class="card" style="width: 100%">
                <div class="card-header">
                    <h3 class="card-title clear-fix" style="width: 100%;">
                        <span class="float-left">Kooperacja</span>
                        <span class="float-right"><i class="fas fa-user-check"></i>&nbsp; {{ $stats['coop']['win'] }} &nbsp; &nbsp; <i class="fas fa-user-times"></i>&nbsp; {{ $stats['coop']['lose'] }}</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="chart-responsive">
                                <canvas id="pieChartCoop1" height="250"></canvas>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="chart-responsive">
                                <canvas id="pieChartCoop2" height="250"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer bg-white p-0">
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="nav nav-pills flex-column margin-top-20">
                                @if(!empty($stats['coop']['top_players'][0]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: gold;"></i> &nbsp; {{$stats['coop']['top_players'][0]['name']}}
                                        <strong class="float-right">{{$stats['coop']['top_players'][0]['score']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['coop']['top_players'][1]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.5"></i> &nbsp; {{$stats['coop']['top_players'][1]['name']}}
                                        <strong class="float-right">{{$stats['coop']['top_players'][1]['score']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['coop']['top_players'][2]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.4"></i> &nbsp; {{$stats['coop']['top_players'][2]['name']}}
                                        <strong class="float-right">{{$stats['coop']['top_players'][2]['score']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['coop']['top_players'][3]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.3"></i> &nbsp; {{$stats['coop']['top_players'][3]['name']}}
                                        <strong class="float-right">{{$stats['coop']['top_players'][3]['score']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['coop']['top_players'][4]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.2"></i> &nbsp; {{$stats['coop']['top_players'][4]['name']}}
                                        <strong class="float-right">{{$stats['coop']['top_players'][4]['score']}}</strong>
                                    </span>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="nav nav-pills flex-column margin-top-20">
                                @if(!empty($stats['coop']['top_games'][0]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: gold;"></i> &nbsp; {{$stats['coop']['top_games'][0]['name']}}
                                        <strong class="float-right">{{$stats['coop']['top_games'][0]['count']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['coop']['top_games'][1]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.5"></i> &nbsp; {{$stats['coop']['top_games'][1]['name']}}
                                        <strong class="float-right">{{$stats['coop']['top_games'][1]['count']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['coop']['top_games'][2]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.4"></i> &nbsp; {{$stats['coop']['top_games'][2]['name']}}
                                        <strong class="float-right">{{$stats['coop']['top_games'][2]['count']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['coop']['top_games'][3]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.3"></i> &nbsp; {{$stats['coop']['top_games'][3]['name']}}
                                        <strong class="float-right">{{$stats['coop']['top_games'][2]['count']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['coop']['top_games'][4]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.2"></i> &nbsp; {{$stats['coop']['top_games'][4]['name']}}
                                        <strong class="float-right">{{$stats['coop']['top_games'][2]['score']}}</strong>
                                    </span>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-md-4 d-flex align-items-stretch">
            <div class="card" style="width: 100%">
                <div class="card-header">
                    <h3 class="card-title clear-fix" style="width: 100%;">
                        <span class="float-left">Solo</span>
                        <span class="float-right"><i class="fas fa-user-check"></i>&nbsp; {{ $stats['solo']['win'] }} &nbsp; &nbsp; <i class="fas fa-user-times"></i>&nbsp; {{ $stats['solo']['lose'] }} &nbsp; &nbsp; <i class="fas fa-ghost"></i>&nbsp; {{ $stats['solo']['no_score'] }}</span>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="chart-responsive">
                                <canvas id="pieChartSolo1" height="250"></canvas>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="chart-responsive">
                                <canvas id="pieChartSolo2" height="250"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer bg-white p-0">
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="nav nav-pills flex-column margin-top-20">
                                @if(!empty($stats['solo']['top_players'][0]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: gold;"></i> &nbsp; {{$stats['solo']['top_players'][0]['name']}}
                                        <strong class="float-right">{{$stats['solo']['top_players'][0]['score']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['solo']['top_players'][1]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.5"></i> &nbsp; {{$stats['solo']['top_players'][1]['name']}}
                                        <strong class="float-right">{{$stats['solo']['top_players'][1]['score']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['solo']['top_players'][2]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.4"></i> &nbsp; {{$stats['solo']['top_players'][2]['name']}}
                                        <strong class="float-right">{{$stats['solo']['top_players'][2]['score']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['solo']['top_players'][3]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.3"></i> &nbsp; {{$stats['solo']['top_players'][3]['name']}}
                                        <strong class="float-right">{{$stats['solo']['top_players'][3]['score']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['solo']['top_players'][4]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.2"></i> &nbsp; {{$stats['solo']['top_players'][4]['name']}}
                                        <strong class="float-right">{{$stats['solo']['top_players'][4]['score']}}</strong>
                                    </span>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="nav nav-pills flex-column margin-top-20">
                                @if(!empty($stats['solo']['top_games'][0]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: gold;"></i> &nbsp; {{$stats['solo']['top_games'][0]['name']}}
                                        <strong class="float-right">{{$stats['solo']['top_games'][0]['count']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['solo']['top_games'][1]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.5"></i> &nbsp; {{$stats['solo']['top_games'][1]['name']}}
                                        <strong class="float-right">{{$stats['solo']['top_games'][1]['count']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['solo']['top_games'][2]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.4"></i> &nbsp; {{$stats['solo']['top_games'][2]['name']}}
                                        <strong class="float-right">{{$stats['solo']['top_games'][2]['count']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['solo']['top_games'][3]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.3"></i> &nbsp; {{$stats['solo']['top_games'][3]['name']}}
                                        <strong class="float-right">{{$stats['solo']['top_games'][2]['count']}}</strong>
                                    </span>
                                    </li>
                                @endif
                                @if(!empty($stats['solo']['top_games'][4]))
                                    <li class="nav-item">
                                    <span class="nav-link">
                                        <i class="nav-icon fas fa-trophy" style="color: black; opacity: 0.2"></i> &nbsp; {{$stats['solo']['top_games'][4]['name']}}
                                        <strong class="float-right">{{$stats['solo']['top_games'][2]['score']}}</strong>
                                    </span>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endif

    <table class="table table-grid radius-border">
        <thead>
        <tr>
            <th>Gra</th>
            <th>Rodzaj</th>
            <th>Data</th>
            <th>Wygrana</th>
            <th></th>
        </tr>
        </thead>
        <tbody class="sortable">

        @foreach ($grid as $key => $item)
            <tr>
                <td>{{$item->name}}</td>
                <td>
                    @if($item->type == 0)
                        Rywalizacja
                    @endif
                    @if($item->type == 1)
                        Kooperacja
                    @endif
                    @if($item->type == 2)
                        Solo
                    @endif
                </td>
                <td>{{$item->date}}</td>
                <td>
                    @if($item->type == 0)
                        <i class="nav-icon fas fa-trophy "></i> {{$item->wins}}
                    @endif
                    @if($item->type == 1)
                        @if($item->win == 1)
                            <i class="fas fa-thumbs-up"></i> {{$item->wins}}
                        @else
                            <i class="fas fa-thumbs-down"></i> {{$item->wins}}
                        @endif
                    @endif
                        @if($item->type == 2 && !empty($item->wins))
                            <i class="nav-icon fas fa-trophy "></i> {{$item->wins}}
                        @endif
                </td>
                <td class="text-right ui-sortable-helper-hide width-150">
                    @if(empty($filter_game))
                        <a class="btn btn-sm btn-success" href="{{ route('admin.board_games_plays.filters.game', $item->board_game_id) }}" title="Wyniki"><i class="fas fa-trophy"></i></a>
                    @endif
                    <a class="btn btn-sm btn-primary" href="{{ route('admin.board_games_plays.edit', $item->id) }}" title="Edytuj"><i class="fas fa-pencil-alt"></i></a>
                    <a class="btn btn-sm btn-danger jq-popup-congrim" data-submit-class="jq-delete-items-form-{{ $item->id }}" href="#" title="Usuń"><i class="fas fa-trash-alt"></i></a>
                    <form method="POST" action="{{ route('admin.board_games_plays.destroy', $item->id) }}" class="jq-delete-items-form-{{ $item->id }}">
                        <input name="_method" type="hidden" value="DELETE">
                        @csrf
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $grid->render() }}

@endsection