@extends('admin.layout')

@section('content')

    <table class="table table-grid radius-border">
        <tbody>
            @foreach ($grid as $menu)
                <tr class="table-header">
                    <th colspan="6">{{ $menu['name'] }}</th>
                </tr>
                <tr>
                    <th style="width: 20px;"><i class="far fa-folder-open"></i></th>
                    <th></th>
                    <th>Tytuł</th>
                    <th>Url</th>
                    <th>Moduł</th>
                    <th></th>
                </tr>
                @if(!empty($menu['items']))
                    @foreach ($menu['items'] as $keyi => $itemi)
                        @php
                            $key = $keyi;
                            $item = $itemi;
                        @endphp
                        @include('admin.urls._index-row')
                    @endforeach
                @endif
            @endforeach
        </tbody>
    </table>

@endsection