@extends('admin.layout')

@section('content')

    <form method="POST" action="{{ route('admin.urls.create.submit') }}" class="card-padding-bottom">
        @csrf
        <div class="card-header silver-border">
            Dane podstawowe
        </div>
        <div class="card-body white-background silver-border radius-border-top">

            <input type="hidden" value="{{ $app['languages_id'] }}" name="languages_id" id="languages_id" class="jq-languages_id" />

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'menus_id', 'input-error jq-input-error') }}">
                <label for="menus_id">Menu</label>
                <select class="form-control jq-menus_id" name="menus_id" data-old="@if(!empty(old('menus_id'))){{ old('menus_id') }}@endif">
                </select>
            </div>

            <div class="form-group">
                <label for="root">Położenie</label>
                <select class="form-control jq-root" name="parent_id" data-old="@if(!empty(old('parent_id'))){{ old('parent_id') }}@endif">
                    {{--@foreach($roots as $key => $value)
                        <option value="{{ $key }}" @if(old('parent_id') == $key) selected @endif> {{ $value }}</option>
                    @endforeach--}}
                </select>
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'modules_id', 'input-error jq-input-error') }}">
                <label for="modules_id">Moduł</label>
                <select class="form-control jq-modules_id" name="modules_id">
                    @foreach($modules as $key => $value)
                        <option value="{{ $key }}" @if(old('modules_id') == $key) selected @endif @if($value['available'] == false) disabled @endif>{{ $value['name'] }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'name', 'input-error jq-input-error') }}">
                <label for="name">Nazwa</label>
                <input type="text" class="form-control jq-create-slug-from" data-slug-to="jq-create-slug-to" id="name" name="name" placeholder="" value="{{ old('name') }}">
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'url', 'input-error jq-input-error') }}">
                <label for="url">Url</label>
                <input type="text" class="form-control jq-check-url-urls jq-create-slug-to" id="url" name="url" placeholder="" value="{{ old('url') }}">
                <div class="jq-check-url-urls-result"></div>
            </div>
        </div>
        <div class="card-header silver-border">
            Meta dane
        </div>
        <div class="card-body white-background silver-border">

            <div class="form-group">
                <label for="meta_title">Tytuł</label>
                <input type="text" class="form-control" id="meta_title" name="meta_title" placeholder="" value="{{ old('meta_title') }}">
            </div>

            <div class="form-group">
                <label for="meta_description">Opis</label>
                <input type="text" class="form-control" id="meta_description" name="meta_description" placeholder="" value="{{ old('meta_description') }}">
            </div>

            <div class="form-group">
                <label for="meta_key_words">Słowa kluczowe</label>
                <input type="text" class="form-control" id="meta_key_words" name="meta_key_words" placeholder="" value="{{ old('meta_key_words') }}">
            </div>
        </div>
        <div class="card-footer silver-border radius-border-bottom">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </form>

@endsection

@include('admin.urls._scripts')