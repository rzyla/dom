@section('scripts')
    <script>

        var id = '{{ $id }}';

        $(document).ready(function()
        {
            /*$('.jq-languages_id').change(function(e)
            {
                updateMenuSelect();
            });*/

            $('.jq-menus_id').change(function(e)
            {
                updateMenuEntries();
            });

            $('.jq-create-slug-from').keypress($.debounce(100, function(e) 
            {
                var dataTo = $(this).attr('data-slug-to');

                if(dataTo != '' && id == 0)
                {
                    $.ajax(
                    {
                        type: 'POST',
                        url: ajaxGenerateUrlPostUrl,
                        data: 
                        {
                            id: id,
                            string: $(this).val()
                        },
                        success:function(data)
                        {
                            $('.' + dataTo).val(data);
                        }
                    });
                }
            }));

            $('.jq-check-url-urls').keypress($.debounce(500, function(e) 
            {
                $('.jq-check-url-urls-result').hide();

                $.ajax(
                {
                    type: 'POST',
                    url: ajaxCheckUrlPostUrl,
                    data: 
                    {
                        id: id,
                        url: $(this).val()
                    },
                    success:function(data)
                    {
                        if(data == true)
                        {
                            $('.jq-check-url-urls-result').text('Adres url dostępny').css('color', 'green');
                        }
                        else
                        {
                            $('.jq-check-url-urls-result').text('Adres url zajęty').css('color', 'red');
                        }

                        $('.jq-check-url-urls-result').show();
                    }
                });
            }));

            updateMenuSelect();
        });

        function updateMenuSelect()
        {

            $.ajax(
            {
                type: 'POST',
                url: ajaxGetMenusPostUrl,
                data: 
                {
                    //id: $('.jq-languages_id option:selected').val(),
                    id: $('.jq-languages_id').val(),
                    label: null
                },
                success:function(data)
                {
                    var html = '';

                    for(var key in data) 
                    {
                        html += '<option value=' + key  + '>' +data[key] + '</option>';
                    }

                    $('.jq-menus_id').find('option').remove().end().append(html).val($('.jq-menus_id').attr('data-old'));
                    
                    updateMenuEntries();
                }
            });
        }

        function updateMenuEntries()
        {
            $.ajax(
            {
                type: 'POST',
                url: ajaxGetMenuPostUrl,
                data: 
                {
                    id: $("option:selected", $('.jq-menus_id')).val(),
                    without: id
                },
                success:function(data)
                {
                    var html = '<option value="0"> - Główny element</option>';

                    for(var key in data) 
                    {
                       html += '<option value=' + key  + '>' +data[key] + '</option>';
                    }

                    $('.jq-root').find('option').remove().end().append(html).val($('.jq-root').attr('data-old'));
                }
            });
        }
    </script>
@endsection