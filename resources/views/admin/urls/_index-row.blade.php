
<tr>
    <td>
        @if(empty($item['parent_id']))
            <img src="{{ asset('assets/admin/images/dits1.png') }}" alt="" />
        @else
            <img src="{{ asset('assets/admin/images/dits3.png') }}" alt="" />
        @endif
    </td>
    <td>
        {{-- $item['level'] --}}
        
        @if(empty($item['parent_id']))
            @if(empty($item['items']))
                <i class="far fa-sticky-note"></i>
            @else
                <i class="far fa-folder-open"></i>
            @endif
        @else
            <i class="far fa-folder-open"></i>
        @endif
    </td>
    <td>
        {{$item['name']}}
    </td>
    <td>
        @if(!empty($item['url']))
            {{ $item['url'] }}
        @endif
    </td>
    <td>
        @if(!empty($modules))
            {{ $modules[$item['menus_id']] }}
        @endif
    </td>
    <td class="text-right">
        @if(!empty($item['route']))
            <a class="btn btn-sm btn-success margin-right-10" href="{{ $item['route'] }}"><i class="fas fa-edit"></i> Aktualizuj</a>
        @endif
        <a class="btn btn-sm btn-primary" href="{{ route('admin.urls.edit', $item['id']) }}" title="Edytuj"><i class="fas fa-pencil-alt"></i></a>
        <a class="btn btn-sm btn-danger jq-popup-congrim" data-submit-class="jq-delete-items-form-{{ $item['id'] }}" href="#" title="Usuń"><i class="fas fa-trash-alt"></i></a>
        <form method="POST" action="{{ route('admin.urls.destroy', $item['id']) }}" class="jq-delete-items-form-{{ $item['id'] }}">
            <input name="_method" type="hidden" value="DELETE">
            @csrf
        </form>
    </td>
</tr>

{{--@if(!empty($item['items']))
    @foreach ($item['items'] as $keyi => $itemi)
        @php
            $key = $keyi;
            $item = $itemi;
        @endphp
        @include('admin.urls._index-row')
    @endforeach
@endif--}}