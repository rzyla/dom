@extends('admin.layout')

@section('scripts')

    <script type="text/javascript">
        $( document ).ready(function()
        {
            $(".jq-on-change-submit#year option:first-child").remove();
        });
    </script>

@endsection

@section('content')

    <table class="table table-grid radius-border">
        <thead>
        <tr>
            <th>Gra</th>
            <th>Ile gier</th>
            <th></th>
        </tr>
        </thead>
        <tbody class="sortable">

        @foreach ($grid as $key => $item)
            @if($item->plays > 0)
            <tr>
                <td>{{$item->name}}</td>
                <td>{{$item->plays}}</td>
            </tr>
            @endif
        @endforeach
        </tbody>
    </table>

    <table class="table table-grid radius-border">
        <thead>
        <tr>
            <th>Gra</th>
            <th>Ile gier</th>
            <th></th>
        </tr>
        </thead>
        <tbody class="sortable">

        @foreach ($grid as $key => $item)
            @if($item->plays == 0)
                <tr>
                    <td>{{$item->name}}</td>
                    <td>{{$item->plays}}</td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>

@endsection
