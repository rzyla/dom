@extends('admin.layout')

@section('content')

    <form method="POST" action="{{ route('admin.notes.create.submit') }}">
        @csrf
        <div class="card-body white-background silver-border radius-border-top">

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'title', 'input-error jq-input-error') }}">
                <label for="name">Tytuł</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="" value="{{ old('title') }}">
            </div>

            <div class="form-group">
                <label for="email">Grupa</label>
                <select class="form-control" name="notes_groups_id">
                    @foreach($notesGroups as $key => $val)
                        <option value="{{ $key }}">{{ $val }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'description', 'input-error jq-input-error') }}">
                <label for="email">Opis</label>
                <textarea class="summernote400 form-control" name="description" id="description" placeholder="">{{ !empty(old('description')) ? old('description') : "" }}</textarea>
            </div>

            <div class="form-group">
                <label for="dashboard">Pokaż na stronie głównej</label>
                <select class="form-control" name="dashboard">
                    <option value="0">Nie</option>
                    <option value="1">Tak</option>
                </select>
            </div>

        </div>
        <div class="card-footer silver-border radius-border-bottom">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </form>

@endsection