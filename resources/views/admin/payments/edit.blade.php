@extends('admin.layout')

@section('content')

    <form method="POST" action="{{ route('admin.notes.edit.submit', $edit->id) }}">
        @csrf
        <input name="_method" type="hidden" value="PUT">
        <div class="card-body white-background silver-border radius-border-top">

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'title', 'input-error jq-input-error') }}">
                <label for="title">Tytuł</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="" value="{{ !empty(old('title')) ? old('title') : $edit->title }}">
            </div>

            <div class="form-group">
                <label for="email">Grupa</label>
                <select class="form-control" name="notes_groups_id">
                    @foreach($notesGroups as $key => $val)
                    <option value="{{ $key }}" @if($key == $edit->notes_groups_id) selected @endif>{{ $val }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'description', 'input-error jq-input-error') }}">
                <label for="email">Opis</label>
                <textarea class="summernote400 form-control" name="description" id="description" placeholder="">{{ !empty(old('description')) ? old('description') : $edit->description }}</textarea>
            </div>

            <div class="form-group">
            <label for="dashboard">Pokaż na stronie głównej</label>
                <select class="form-control" name="dashboard">
                    <option value="0" @if(0 == $edit->dashboard) selected @endif>Nie</option>
                    <option value="1" @if(1 == $edit->dashboard) selected @endif>Tak</option>
                </select>
            </div>

        </div>
        <div class="card-footer silver-border radius-border-bottom">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </form>

@endsection