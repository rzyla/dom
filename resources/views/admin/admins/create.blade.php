@extends('admin.layout')

@section('content')

    <form method="POST" action="{{ route('admin.admins.create.submit') }}">
        @csrf
        <div class="card-body white-background silver-border radius-border-top">

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'name', 'input-error jq-input-error') }}">
                <label for="name">Imię i nazwisko</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ old('name') }}">
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'email', 'input-error jq-input-error') }}">
                <label for="email">Adres e-mail</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="" value="{{ old('email') }}">
            </div>

            <div class="form-group">
                <label for="email">Typ konta</label>
                <select class="form-control" name="acl">
                    @foreach($acl as $key => $val)
                        <option value="{{ $key }}">{{ $val }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'password', 'input-error jq-input-error') }}">
                <label for="password">Hasło</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="">
                <div><small>Minimalna ilość znaków: 6</small></div>
            </div>

        </div>
        <div class="card-footer silver-border radius-border-bottom">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </form>

@endsection