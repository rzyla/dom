<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="{{ route('admin.account.edit', $app['user']['id']) }}" class="nav-link">Moje konto</a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
          <a class="logout-link" href="{{ route('admin.logout') }}">Wyloguj <i class="fas fa-sign-out-alt"></i></a>
      </li>
    </ul>
</nav>