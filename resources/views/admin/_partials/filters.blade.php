@if(!empty($app['filters']))

    <div class="filters">

        <div class="filters-label">Filtruj: </div>

        @foreach($app['filters'] as $key => $filter)
            <div class="filters-item">
                <form method="POST" action="{{ $filter['route'] }}">
                 @csrf
                    <select class="form-control jq-on-change-submit" name="{{ $key }}" id="{{ $key }}">
                        <option value="">{{ $filter['default'] }}</option>

                        @foreach($filter['items'] as $key => $item)
                            @if(empty($filter['name']))
                                <option value="{{ $key }}" @if($key == $filter['selected']) selected @endif >{{ $item }}</option>
                            @else
                                <option value="{{ $key }}" @if($key == $filter['selected']) selected @endif >{{ $filter['name'] }}: {{ $item }}</option>
                            @endif
                        @endforeach
                    </select>
                </form>
            </div>
        @endforeach

    </div>

@endif
