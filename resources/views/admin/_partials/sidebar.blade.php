<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="http://www.arteh.pl" target="_blank" class="brand-link">
        <img src="{{ asset('assets/admin/images/arteh.png') }}" alt="ARTEHCMS" class="brand-image">
        <span class="brand-text">ARTEH</span><span class="brand-text font-weight-light">CMS</span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3">
        <div class="image">
          <a href="{{ route('admin.account.edit', $app['user']['id']) }}">
            <img src="{{ ImageHelper::Thumb($app['user']['avatar'], 50, 50) }}" class="img-circle" alt="{{ $app['user']['name'] }}">
          </a>
        </div>
        <div class="info">
          <a href="{{ route('admin.account.edit', $app['user']['id']) }}" class="d-block">{{ $app['user']['name'] }}</a>
        </div>
      </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @foreach ($app['menu'] as $parent)
                    @if($parent['visible'] == true)
                        <li class="nav-item @if(!empty($parent['children'])) has-treeview @endif @if($parent['active'] == true) menu-open @endif ">
                            <a href="{{ $parent['route'] }}" class="nav-link @if($parent['active'] == true) active @endif ">
                                @if(!empty($parent['icon']))
                                    <i class="nav-icon fas {{ $parent['icon'] }}"></i>
                                @endif
                                <p>
                                    {{ $parent['name'] }}
                                    @if(!empty($parent['children']))
                                        <i class="fas fa-angle-left right"></i>
                                    @endif
                                </p>
                            </a>
                            @if(!empty($parent['children']))
                                <ul class="nav nav-treeview" style="background-color: rgba(255,255,255,.05); border-radius: 5px;">
                                    @foreach ($parent['children'] as $child)
                                        @if($child['visible'] == true)
                                            <li class="nav-item">
                                                <a href="{{ $child['route'] }}" class="nav-link @if($child['active'] == true) active @endif ">
                                                    @if(!empty($child['icon']))
                                                        <i class="nav-icon fas {{ $child['icon'] }} "></i>
                                                    @endif
                                                    <p>
                                                        {{ $child['name'] }}
                                                    </p>
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endif
                @endforeach
            </ul>
        </nav>
    </div>
</aside>