@if(!empty($app['search']))

    <div class="search">
        <form method="POST" action="{{ $app['buttons']['search'] }}">
            @csrf

            <div class="float-sm-right" style="margin-left: 4px;">
                <a class="btn btn-warning jq-on-click-submit" href="#" title="Szukaj"><i class="fas fa-search"></i></a>
            </div>
            <div class="float-sm-right">
                <input type="text" class="form-control" name="search" placeholder="Szukaj" value="{{$app['searchString']}}" />
            </div>

        </form>
    </div>

@endif
