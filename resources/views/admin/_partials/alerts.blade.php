@if ($message = Session::get('success'))
	<div class="alert alert-success alert-dismissible fade show" role="alert">
		{{ $message }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	</div>
@endif

@if ($message = Session::get('error'))
	<div class="alert alert-danger alert-dismissible fade show" role="alert">
		{{ $message }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	</div>
@endif
	
@if (count($errors) > 0)
	<div class="alert alert-danger alerts-top">
		<strong>Uwaga!</strong> Wypełnij poprawnie wszystkie pola:
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif