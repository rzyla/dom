<script>
    var csrfToken = '{{ csrf_token() }}';
    var ajaxGetMenusPostUrl = '{{ route('admin.menus.ajaxGetMenus') }}';
    var ajaxGenerateUrlPostUrl = '{{ route('admin.urls.ajaxGenerateUrl') }}';
    var ajaxCheckUrlPostUrl = '{{ route('admin.urls.ajaxCheckUrl') }}';
    var ajaxGetMenuPostUrl = '{{ route('admin.urls.ajaxGetMenu') }}';
</script>

<script src="{{ asset('assets/admin/js/admin.js') }}"></script>

<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>