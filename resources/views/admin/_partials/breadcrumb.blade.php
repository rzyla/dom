<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-10">
                <h1 class="m-0 text-dark">@if(!empty($app['current']['title'])) {{ $app['current']['title'] }} @endif</h1>
            </div>
            <div class="col-sm-2">
                <div class="float-sm-right">
                    @if(!empty($app['buttons']['create']))
                        <a class="btn btn-primary" href="{{ $app['buttons']['create'] }}" title="Dodaj nowy element"><i class="fas fa-plus"></i></a>
                    @endif
                    @if(!empty($app['buttons']['index']))
                        <a class="btn btn-default" href="{{ $app['buttons']['index'] }}" title="Dodaj nowy element"><i class="fas fa-bars"></i></a>
                    @endif
                </div>

                {{--
                <ol class="breadcrumb float-sm-right">

                    @foreach($app['breadcrumbs'] as $breadcrumb)
                        <li class="breadcrumb-item @if($breadcrumb['active'] == true) active @endif">

                            @if($breadcrumb['active'] == false) <a href="{{ $breadcrumb['route'] }}"> @endif

                                {{ $breadcrumb['name'] }}

                            @if($breadcrumb['active'] == false) </a> @endif
                        </li>
                    @endforeach

                </ol>
                --}}
            </div>

            @if(empty($app['search']))
                <div class="col-sm-12">
            @else
               <div class="col-sm-9">
            @endif
                    @include('admin._partials.filters')
               </div>

            @if(!empty($app['search']))
                <div class="col-sm-3">
                    @include('admin._partials.search')
                </div>
            @endif

        </div>
    </div>
 </div>
