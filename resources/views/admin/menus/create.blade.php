@extends('admin.layout')

@section('content')

    <form method="POST" action="{{ route('admin.menus.create.submit') }}">
        @csrf
        <div class="card-body white-background silver-border radius-border-top">

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'name', 'input-error jq-input-error') }}">
                <label for="name">Nazwa</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ old('name') }}">
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'symbol', 'input-error jq-input-error') }}">
                <label for="email">Symbol</label>
                <input type="text" class="form-control" id="symbol" name="symbol" placeholder="" value="{{ old('symbol') }}">
            </div>

            <div class="form-group">
                <label for="email">Język</label>
                <select class="form-control" name="languages_id">
                    @foreach($app['languages'] as $key => $val)
                        <option value="{{ $key }}">{{ $val }}</option>
                    @endforeach
                </select>
            </div>

        </div>
        <div class="card-footer silver-border radius-border-bottom">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </form>

@endsection