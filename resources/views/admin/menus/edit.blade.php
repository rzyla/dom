@extends('admin.layout')

@section('content')

    <form method="POST" action="{{ route('admin.menus.edit.submit', $edit->id) }}">
        @csrf
        <input name="_method" type="hidden" value="PUT">
        <div class="card-body white-background silver-border radius-border-top">

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'name', 'input-error jq-input-error') }}">
                <label for="name">Imię i nazwisko</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ !empty(old('name')) ? old('name') : $edit->name }}">
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'symbol', 'input-error jq-input-error') }}">
                <label for="email">Symbol</label>
                <input type="text" class="form-control" id="symbol" name="symbol" placeholder="" value="{{ !empty(old('symbol')) ? old('symbol') : $edit->symbol }}">
            </div>

            <div class="form-group">
                <label for="email">Język</label>
                <select class="form-control" name="languages_id">
                    @foreach($app['languages'] as $key => $val)
                        <option value="{{ $key }}" @if($key == $edit->languages_id) selected @endif>{{ $val }}</option>
                    @endforeach
                </select>
            </div>

        </div>
        <div class="card-footer silver-border radius-border-bottom">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </form>

@endsection