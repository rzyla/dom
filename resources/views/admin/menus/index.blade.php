@extends('admin.layout')

@section('content')

    <table class="table table-grid radius-border">
        <tbody>
            <tr>
                <th>Nazwa</th>
                <th>Symbol</th>
                <th>Język</th>
                <th></th>
            </tr>
            @foreach ($grid as $key => $item)
            <tr>
                <td>{{ $item->name }}</td>
                <td>{{ $item->symbol }}</td>
                <td>{{ $app['languages'][$item->languages_id] }}</td>
                <td class="text-right">
                    <a class="btn btn-sm btn-primary" href="{{ route('admin.menus.edit', $item->id) }}" title="Edytuj"><i class="fas fa-pencil-alt"></i></a>
                    <a class="btn btn-sm btn-danger jq-popup-congrim" data-submit-class="jq-delete-items-form-{{ $item->id }}" href="#" title="Usuń"><i class="fas fa-trash-alt"></i></a>
                    <form method="POST" action="{{ route('admin.menus.destroy', $item->id) }}" class="jq-delete-items-form-{{ $item->id }}">
                        <input name="_method" type="hidden" value="DELETE">
                        @csrf
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {{ $grid->render() }}

@endsection