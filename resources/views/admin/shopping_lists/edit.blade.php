@extends('admin.layout')

@section('content')

    <form method="POST" action="{{ route('admin.shopping_lists.edit.submit', $edit->id) }}" enctype="multipart/form-data">
        @csrf
        <input name="_method" type="hidden" value="PUT">
        <div class="card-body white-background silver-border radius-border-top">

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'name', 'input-error jq-input-error') }}">
                <label for="name">Nazwa</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ !empty(old('name')) ? old('name') : $edit->name }}">
            </div>

            <div class="form-group">
                <label for="shops">Sklep</label>
                @foreach($shoppingListsShops as $key => $val)
                    <div class="custom-control custom-checkbox">
                        <input name="shops[]" class="custom-control-input" type="checkbox" id="shoppingListsShops-{{ $key }}" value="{{ $key }}" @if(!empty($shoppingListsShopsShoppingLists[$key])) checked @endif>
                        <label for="shoppingListsShops-{{ $key }}" class="custom-control-label">{{ $val }}</label>
                    </div>
                @endforeach
            </div>

            <div class="form-group">
                <label for="quantity">Ilość</label>
                <input min="0" type="number" step="1" class="form-control" id="quantity" name="quantity" placeholder="" value="{{ !empty(old('quantity')) ? old('quantity') : $edit->quantity }}">
            </div>

            <div class="form-group">
                <label for="prio">Ważne</label>
                <div class="custom-control custom-checkbox">

                    @if(!empty(old('prio')))
                        <input class="custom-control-input" type="checkbox" name="prio" id="prio" value="1" @if(!empty(old('prio'))) checked @endif>
                    @else
                        <input class="custom-control-input" type="checkbox" name="prio" id="prio" value="1" @if(!empty($edit->prio)) checked @endif>
                    @endif
                    <label for="prio" class="custom-control-label">Oznacz jako ważne</label>
                </div>
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'lead', 'input-error jq-input-error') }}">
                <label for="email">Krótki opis</label>
                <textarea class="form-control" name="lead" id="lead" placeholder="">{{ !empty(old('lead')) ? old('lead') : $edit->lead }}</textarea>
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'description', 'input-error jq-input-error') }}">
                <label for="email">Opis</label>
                <textarea class="summernote400 form-control" name="description" id="description" placeholder="">{{ !empty(old('description')) ? old('description') : $edit->description }}</textarea>
            </div>

            <div class="form-group">
                <label for="exampleInputFile">Zdjęcie</label>
                @if(empty($edit->image))
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="image" name="image" accept="image/*">
                        <label class="custom-file-label" for="image"></label>
                    </div>
                </div>
                @else
                <div>
                     <img src="{{ ImageHelper::Thumb($app['current']['uploadSourceDir'].$edit->image, 200, 200) }}" alt="" />
                </div>
                <input type="checkbox" value="delete" name="deleteImage" /> Usuń
                @endif
            </div>

        </div>
        <div class="card-footer silver-border radius-border-bottom">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </form>

@endsection
