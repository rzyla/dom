@extends('admin.layout')

@section('content')

    <table class="table table-grid radius-border">
        <thead>
            <tr>
                <th></th>
                <th>Nazwa</th>
                <th>Sklepy</th>
                <th>Ilość</th>
                <th>Ważne</th>
                @if(!empty($app['arrows']))
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                @endif
                <th></th>
            </tr>
        </thead>
        <tbody class="sortable">
            @foreach ($grid as $key => $item)
            <tr>
                <td class="width-50">
                    @if(!empty($item->image))
                        <img src="{{ ImageHelper::Thumb($app['current']['uploadSourceDir'].$item->image, 50, 50) }}" alt="" />
                    @endif
                </td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->shops }}</td>
                <td class="product-update width-250">
                    <form method="POST" action="{{ route('admin.shopping_lists.ajax_quantity_priority.submit') }}">
                        <input type="text" class="form-control jq-numeric" name="quantity" value="{{ $item->quantity }}" data-id="{{$item->id}}">
                        <a class="btn btn-sm jq-ajax-query-quantity" href="{{ route('admin.shopping_lists.ajax_quantity_priority.submit') }}" title="Aktualizuj"><i class="fas fa-exchange-alt"></i></a>
                    </form>
                </td>
                <td class="width-50">
                    <form method="POST" action="{{ route('admin.shopping_lists.ajax_quantity_priority.submit') }}">
                        <div class="custom-control custom-switch">
                            @if($item->prio == 1)
                                <input type="checkbox" class="custom-control-input jq-ajax-query-priority" id="customSwitch-{{$item->id}}" data-id="{{$item->id}}" data-href="{{ route('admin.shopping_lists.ajax_quantity_priority.submit') }}" checked>
                            @else
                                <input type="checkbox" class="custom-control-input jq-ajax-query-priority" id="customSwitch-{{$item->id}}" data-id="{{$item->id}}" data-href="{{ route('admin.shopping_lists.ajax_quantity_priority.submit') }}">
                            @endif
                            <label class="custom-control-label" for="customSwitch-{{$item->id}}">&nbsp;</label>
                        </div>
                    </form>
                </td>
                @if(!empty($app['arrows']))
                    <td class="width-50"><a href="{{ route('admin.shopping_lists.up_down', ['action' => 'top', 'id' => $item->id]) }}" class="jq-arrow-up"><i class="fas fa-angle-double-up"></i></a></td>
                    <td class="width-50"><a href="{{ route('admin.shopping_lists.up_down', ['action' => 'up', 'id' => $item->id]) }}" class="jq-arrow-up" data-id="{{$item->id}}"><i class="fas fa-arrow-up"></i></a></td>
                    <td class="width-50"><a href="{{ route('admin.shopping_lists.up_down', ['action' => 'down', 'id' => $item->id]) }}" class="jq-arrow-down" data-id="{{$item->id}}"><i class="fas fa-arrow-down"></i></a></td>
                    <td class="width-50"><a href="{{ route('admin.shopping_lists.up_down', ['action' => 'bottom', 'id' => $item->id]) }}" class="jq-arrow-down"><i class="fas fa-angle-double-down"></i></a></td>
                @endif
                <td class="text-right ui-sortable-helper-hide width-150">
                    <a class="btn btn-sm btn-primary" href="{{ route('admin.shopping_lists.edit', $item->id) }}" title="Edytuj"><i class="fas fa-pencil-alt"></i></a>
                    <a class="btn btn-sm btn-danger jq-popup-congrim" data-submit-class="jq-delete-items-form-{{ $item->id }}" href="#" title="Usuń"><i class="fas fa-trash-alt"></i></a>
                    <form method="POST" action="{{ route('admin.shopping_lists.destroy', $item->id) }}" class="jq-delete-items-form-{{ $item->id }}">
                        <input name="_method" type="hidden" value="DELETE">
                        @csrf
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {{ $grid->render() }}

@endsection
