<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ARTEH CMS</title>

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
        <link href="{{ asset('assets/admin/css/admin.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/admin/images/favico.ico') }}"  rel="shortcut icon">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">

            <div class="login-logo">
                <a href="http://www.arteh.pl" target="_blank"><b>ARTEH</b>CMS</a>
            </div>
        
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">Podaj dane do logowania</p>

                    <form method="POST" action="{{ route('admin.login.submit') }}" aria-label="{{ __('Login') }}">

                        @csrf
                        <div>
                            <div class="input-group mb-3 {{ AdminHelper::DisplayErrorsClass($errors, 'email', 'input-error jq-input-error') }}">
                                <input type="email" class="form-control" placeholder="Adres e-mail" name="email" id="email" value="{{ old('email') }}">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-envelope"></span>
                                    </div>
                                </div>
                            </div>
                            {{ AdminHelper::DisplayErrorsDiv($errors, 'email', 'Puste lub błędne pole Adres e-mail', 'login-error') }}
                        </div>
                        <div>
                            <div class="input-group mb-3 {{ AdminHelper::DisplayErrorsClass($errors, 'password', 'input-error jq-input-error') }}">
                                <input type="password" class="form-control" placeholder="Hasło" name="password" id="password">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-lock"></span>
                                    </div>
                                </div>
                            </div>
                            {{ AdminHelper::DisplayErrorsDiv($errors, 'password', 'Puste lub błędne pole Hasło', 'login-error') }}
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <div class="icheck-primary">
                                <input type="checkbox" id="remember" name="remember" {{ !empty(old('remember')) ? ' checked' : '' }}>
                                <label for="remember">
                                    Zapamiętaj mnie
                                </label>
                                </div>
                            </div>

                            <div class="col-4">
                                <button type="submit" class="btn btn-primary btn-block">Zaloguj</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script src="{{ asset('assets/admin/js/admin.js') }}"></script>
        
    </body>
</html>