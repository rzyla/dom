@extends('admin.layout')

@section('content')

    <table class="table table-grid radius-border">
        <thead>
            <tr>
                <th>Nazwa</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="sortable">
            @foreach ($grid as $key => $item)
            <tr>
                <td>{{ $item->name }}</td>
                <td class="text-right ui-sortable-helper-hide width-150">
                    <a class="btn btn-sm btn-primary" href="{{ route('admin.board_games_players.edit', $item->id) }}" title="Edytuj"><i class="fas fa-pencil-alt"></i></a>
                    <a class="btn btn-sm btn-danger jq-popup-congrim" data-submit-class="jq-delete-items-form-{{ $item->id }}" href="#" title="Usuń"><i class="fas fa-trash-alt"></i></a>
                    <form method="POST" action="{{ route('admin.board_games_players.destroy', $item->id) }}" class="jq-delete-items-form-{{ $item->id }}">
                        <input name="_method" type="hidden" value="DELETE">
                        @csrf
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {{ $grid->render() }}

@endsection
