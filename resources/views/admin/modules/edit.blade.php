@extends('admin.layout')

@section('content')

    <form method="POST" action="{{ route('admin.modules.edit.submit', $edit->id) }}">
        @csrf
        <input name="_method" type="hidden" value="PUT">
        <div class="card-body white-background silver-border radius-border-top">

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'name', 'input-error jq-input-error') }}">
                <label for="name">Nazwa</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ !empty(old('name')) ? old('name') : $edit->name }}">
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'class', 'input-error jq-input-error') }}">
                <label for="email">Klasa</label>
                <input type="text" class="form-control" id="class" name="class" placeholder="" value="{{ !empty(old('class')) ? old('class') : $edit->class }}">
            </div>

            <div class="form-group {{ AdminHelper::DisplayErrorsClass($errors, 'symbol', 'input-error jq-input-error') }}">
                <label for="email">Symbol</label>
                <input type="text" class="form-control" id="symbol" name="symbol" placeholder="" value="{{ !empty(old('symbol')) ? old('symbol') : $edit->symbol }}">
            </div>

            <div class="form-group">
                <label for="email">Pojedynczy</label>
                <select class="form-control" name="singleton">
                    <option value="0" @if(0 == $edit->singleton) selected @endif>Nie</option>
                    <option value="1" @if(1 == $edit->singleton) selected @endif>Tak</option>
                </select>
            </div>

        </div>
        <div class="card-footer silver-border radius-border-bottom">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </form>

@endsection