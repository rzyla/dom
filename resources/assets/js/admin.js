$(document).ready(function()
{
	$('.jq-input-error input').focus(function(e)
	{
		$(this).parent().removeClass('input-error').parent().find('.login-error').hide();
	});

	$('.jq-popup-congrim').click(function(e)
	{
		e.preventDefault();

		var submitClass = $(this).attr('data-submit-class');

		$('#modal-delete .jq-delete-submit').on('click', function(e)
		{
			$('.' + submitClass).submit();
			$('#modal-delete').modal('hide');
		});

		$('#modal-delete').modal('show');
	});

	$('.jq-on-change-submit').change(function(e)
	{
		$(this).closest('form').submit();
	});

    $('.jq-on-click-submit').click(function(e)
    {
        $(this).closest('form').submit();
    });

    $('.jq-ajax-query-quantity').click(function(e)
    {
        e.preventDefault();

        $('.border-animate').removeClass('border-animate');
        $('.border-animate-error').removeClass('border-animate-error');

        var id = $(this).parent().find('input').attr('data-id');
        var quantity = $(this).parent().find('input').val();

        $(this).parent().find('input').addClass('border-animate');

        $.post( $(this).attr('href'), { id: id, quantity: quantity }, function( data )
        {
            if(data == 'OK') {
                $('.border-animate').removeClass('border-animate');
            }
            else
            {
                $('.border-animate').addClass('border-animate-error').removeClass('border-animate');
            }
        });
    });

    $('.jq-ajax-query-priority').change(function(e)
    {
        e.preventDefault();

        var id = $(this).parent().find('input').attr('data-id');
        var prio = 0;

        if($(this).parent().find('input').is(':checked'))
        {
            prio= 1;
        }

        $.post( $(this).attr('data-href'), { id: id, prio: prio }, function( data )
        {
            if(data != 'OK')
            {
                alert('Błąd podczas aktualizacji priorytetu');
            }
        });
    });

	$('input[type="file"]').on('change',function()
	{
		var val = $(this).val();
		var valSlash = val.replace(/\\/g, '');
		var valFakePath = valSlash.replace('C:fakepath', '');

		$(this).next('.custom-file-label').html(valFakePath);
	});

    $('body').on('keydown', '.jq-numeric', function(event)
    {
        if(event.keyCode != 46 && event.keyCode != 8 && event.keyCode != 9 && event.keyCode != 37 && event.keyCode != 38 && event.keyCode != 39 && event.keyCode != 40 && event.keyCode != 188 && event.keyCode != 190 && event.keyCode != 110)
        {
            if(event.keyCode==190)
            {
                event.preventDefault();
            }
            else
            {
                if(event.keyCode < 48)
                {
                    event.preventDefault();
                }
                else
                {
                    if(event.keyCode > 106)
                    {
                        event.preventDefault();
                    }
                    else
                    {
                        if(event.keyCode > 57)
                        {
                            if (event.keyCode < 95 || event.keyCode > 105 )
                            {
                                event.preventDefault();
                            }
                        }
                    }
                }
            }
        }
    });
});

$.ajaxSetup(
{
    headers: {'X-CSRF-TOKEN': csrfToken }
});
